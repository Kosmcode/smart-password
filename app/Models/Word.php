<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\Word
 *
 * @property int $id
 * @property int $lang_id ID language enum
 * @property string $original Original word from dictionary
 * @property string $prepared Prepared word from dictionary
 * @property int $length Length of word
 * @property int $count_special_chars Count of special chars to replace
 * @property string $available_spec_chars Available special chars to use in word
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @method static Builder|Word newModelQuery()
 * @method static Builder|Word newQuery()
 * @method static Builder|Word query()
 * @method static Builder|Word whereAvailableSpecChars($value)
 * @method static Builder|Word whereCountSpecialChars($value)
 * @method static Builder|Word whereCreatedAt($value)
 * @method static Builder|Word whereId($value)
 * @method static Builder|Word whereLangId($value)
 * @method static Builder|Word whereLength($value)
 * @method static Builder|Word whereOriginal($value)
 * @method static Builder|Word wherePrepared($value)
 * @method static Builder|Word whereUpdatedAt($value)
 *
 * @mixin Eloquent
 */
class Word extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'lang_id',
        'original',
        'prepared',
        'length',
        'available_spec_chars',
        'count_special_chars',
    ];

    public function getId(): int
    {
        return $this->id;
    }
}
