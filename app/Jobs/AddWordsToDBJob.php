<?php

namespace App\Jobs;

use App\Enums\LanguageIdCodeEnum;
use App\Services\Word\AddServiceInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Config\Repository as Config;
use Illuminate\Contracts\Queue\Job;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Filesystem\FilesystemManager as Storage;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Log\LogManager as Log;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AddWordsToDBJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(
        private string $chunkFilepath
    ) {
    }

    /**
     * Execute the job.
     */
    public function handle(
        Storage $storage,
        Log $log,
        Config $config,
        AddServiceInterface $addWordService
    ): void {
        $log = $log->channel('populateWords');

        if (! $this->job instanceof Job) {
            $log->error('Job is not instance of \Illuminate\Contracts\Queue\Job');

            return;
        }

        $log->info('Start job', ['jobId' => $this->job->getJobId()]);

        if ($storage->exists($this->chunkFilepath) === false) {
            $log->error(
                'Not exists chunk file',
                [
                    'filepath' => $this->chunkFilepath,
                    'jobId' => $this->job->getJobId(),
                ]
            );

            return;
        }

        $fileChunkHandle = $storage->readStream($this->chunkFilepath);

        if (is_resource($fileChunkHandle) === false) {
            $log->error(
                'Not cant handle file stream resource',
                [
                    'filepath' => $this->chunkFilepath,
                    'jobId' => $this->job->getJobId(),
                ]
            );

            return;
        }

        $minExpectedSpecChars = $config->get('words.minimumExpectedSpecialChars');

        if (! is_int($minExpectedSpecChars)) {
            $log->error('Config `words.minimumExpectedSpecialChars` is not int');

            return;
        }

        $expectedCharsPattern = '/'.implode('|', array_keys((array) $config->get('words.charToReplace'))).'/';

        $addWordService->setMinExpectedSpecChars($minExpectedSpecChars);
        $addWordService->setExpectedCharsPattern($expectedCharsPattern);

        $filepathExploded = explode(DIRECTORY_SEPARATOR, $this->chunkFilepath);
        $filename = last($filepathExploded);

        if (! is_string($filename)) {
            $log->error('Last element is not a string');

            return;
        }

        $langCode = explode('_', $filename)[0];
        $langId = LanguageIdCodeEnum::from($langCode);
        $addWordService->setLangId((int) $langId->value);

        while (($word = fgets($fileChunkHandle)) !== false) {
            $word = str_replace([' ', PHP_EOL], '', $word);

            $addWordService->addWordToDb($word);
        }

        fclose($fileChunkHandle);

        $storage->delete($this->chunkFilepath);

        $log->info(
            'Done job and deleted chunk file',
            [
                'jobId' => $this->job->getJobId(),
                'filepath' => $this->chunkFilepath,
            ]
        );
    }
}
