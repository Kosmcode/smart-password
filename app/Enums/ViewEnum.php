<?php

namespace App\Enums;

use Spatie\Enum\Enum;

/**
 * @method static self privacyPolicy()
 * @method static self cookiesConsents()
 */
class ViewEnum extends Enum
{
    protected static function values(): array
    {
        return [
            'privacyPolicy' => 'page.privacy_policy',
            'cookiesConsents' => 'page.cookies_consents',
        ];
    }
}
