<?php

namespace App\Enums;

use Spatie\Enum\Enum;

/**
 * @method static self polish()
 * @method static self english()
 * @method static self german()
 * @method static self french()
 * @method static self italian()
 * @method static self spanish()
 */
class WordLanguageEnum extends Enum
{
    protected static function values(): array
    {
        return [
            'polish' => 1,
            'english' => 2,
            'german' => 3,
            'french' => 4,
            'italian' => 5,
            'spanish' => 6,
        ];
    }
}
