<?php

namespace App\Enums;

use Spatie\Enum\Enum;

/**
 * @method static self pl()
 * @method static self en()
 * @method static self de()
 * @method static self fr()
 * @method static self it()
 * @method static self es()
 */
class LanguageIdCodeEnum extends Enum
{
    protected static function values(): array
    {
        return [
            'pl' => WordLanguageEnum::polish()->value,
            'en' => WordLanguageEnum::english()->value,
            'de' => WordLanguageEnum::german()->value,
            'fr' => WordLanguageEnum::french()->value,
            'it' => WordLanguageEnum::italian()->value,
            'es' => WordLanguageEnum::spanish()->value,
        ];
    }
}
