<?php

namespace App\Enums;

use Spatie\Enum\Enum;

/**
 * @method static self dictionaries()
 * @method static self camelCase()
 * @method static self lengthMin()
 * @method static self lengthMax()
 * @method static self passwordsCount()
 * @method static self captchaToken()
 * @method static self characters()
 * @method static self password()
 */
class RequestFieldEnum extends Enum
{
    protected static function values(): array
    {
        return [
            'dictionaries' => 'dictionaries',
            'camelCase' => 'camelCase',
            'lengthMin' => 'lengthMin',
            'lengthMax' => 'lengthMax',
            'passwordsCount' => 'passwordsCount',
            'captchaToken' => 'captchaToken',
            'characters' => 'characters',
            'password' => 'password',
        ];
    }
}
