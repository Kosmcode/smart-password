<?php

namespace App\Enums;

use Spatie\Enum\Enum;

/**
 * @method static self pageMain()
 * @method static self pageSimple()
 */
class InertiaComponentEnum extends Enum
{
    protected static function values(): array
    {
        return [
            'pageMain' => 'Main',
            'pageSimple' => 'PageSimple',
        ];
    }
}
