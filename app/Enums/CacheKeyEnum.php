<?php

namespace App\Enums;

use Spatie\Enum\Enum;

/**
 * @method static self passwordsRandomConfig()
 */
class CacheKeyEnum extends Enum
{
    protected static function values(): array
    {
        return [
            'passwordsRandomConfig' => 'passwords.random.config',
        ];
    }
}
