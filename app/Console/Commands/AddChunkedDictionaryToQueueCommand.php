<?php

namespace App\Console\Commands;

use App\Jobs\AddWordsToDBJob;
use Illuminate\Config\Repository as Config;
use Illuminate\Console\Command;
use Illuminate\Filesystem\FilesystemManager as Storage;
use Illuminate\Queue\QueueManager as Queue;

class AddChunkedDictionaryToQueueCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dictionary:chunked:add-to-queue';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to add chunked dictionary to queue';

    /**
     * Execute the console command.
     */
    public function handle(
        Storage $storage,
        Config $config,
        Queue $queue
    ): int {
        $chunkedFilepath = $config->get('dictionary.dictionariesChunkedPath');

        if (! is_string($chunkedFilepath)) {
            $this->output->error('Error set config value `dictionary.dictionariesChunkedPath`');

            return Command::FAILURE;
        }

        $chunkedFiles = $storage->allFiles($chunkedFilepath);

        if ($chunkedFiles === []) {
            $this->output->warning('Not found chunked files');

            return Command::FAILURE;
        }

        foreach ($chunkedFiles as $filepath) {
            $queue->push(new AddWordsToDBJob($filepath));
        }

        $this->output->success('Done');

        return Command::SUCCESS;
    }
}
