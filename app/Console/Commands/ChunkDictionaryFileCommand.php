<?php

namespace App\Console\Commands;

use Illuminate\Config\Repository as Config;
use Illuminate\Console\Command;
use Illuminate\Filesystem\FilesystemManager as Storage;

class ChunkDictionaryFileCommand extends Command
{
    private const DICTIONARIES_PATH = 'dictionaries/';

    private const DICTIONARIES_CHUNK_PATH = self::DICTIONARIES_PATH.'chunked/';

    private const CHUNK_FILE_COUNT_ROW = 1000;

    private const DICTIONARY_WORDS_SEPARATOR = ',';

    private const LINE_PREPARE_REPLACE_REGEX = '/[0-9]|-|_|\'|\./';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dictionary:chunk {filename}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to chunk dictionary file';

    /**
     * Execute the console command.
     */
    public function handle(
        Storage $storage,
        Config $config
    ): int {
        $dictionaryFilename = $this->argument('filename');

        if (! is_string($dictionaryFilename)) {
            $this->error('Filename must be a string path');

            return Command::FAILURE;
        }

        $dictionaryFilepath = self::DICTIONARIES_PATH.$dictionaryFilename;

        if ($storage->exists($dictionaryFilepath) === false) {
            $this->error('Not exists file: '.$dictionaryFilepath);

            return Command::FAILURE;
        }

        $dictionaryFileHandle = $storage->readStream($dictionaryFilepath);

        if (is_resource($dictionaryFileHandle) === false) {
            $this->error('Dictionary file cant handle');

            return Command::FAILURE;
        }

        $timestamp = time();

        $explodedFilename = explode('.', $dictionaryFilename);

        $chunkCount = 1;

        $chunkFilename = $explodedFilename[0].'_'.$chunkCount.'_'.$timestamp.'.'.$explodedFilename[1];

        $minWordLength = $config->get('words.minimumLength');

        $wordsCount = 1;

        while (($line = fgets($dictionaryFileHandle)) !== false) {
            $words = explode(self::DICTIONARY_WORDS_SEPARATOR, $line);

            if (! isset($words[0])) {
                continue;
            }

            $word = $words[0];

            if ($wordsCount >= self::CHUNK_FILE_COUNT_ROW) {
                $this->info('Create new chunk file');

                $chunkCount++;
                $wordsCount = 0;

                $chunkFilename = $explodedFilename[0].'_'.$chunkCount.'_'.$timestamp.'.'
                    .$explodedFilename[1];
            }

            $preparedWord = (string) preg_replace(self::LINE_PREPARE_REPLACE_REGEX, '', trim($word));

            if (strlen($preparedWord) < $minWordLength) {
                continue;
            }

            $storage->append(self::DICTIONARIES_CHUNK_PATH.$chunkFilename, $preparedWord);

            $wordsCount++;
        }

        $this->output->success('Done!');

        return Command::SUCCESS;
    }
}
