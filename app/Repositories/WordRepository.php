<?php

namespace App\Repositories;

use App\Dtos\Request\Passwords\DictionariesDTO;
use App\Models\Word;

/**
 * Word Repository
 */
class WordRepository implements WordRepositoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function createOrUpdateWord(array $attributes, array $values): Word
    {
        return Word::query()// @phpstan-ignore-line
            ->updateOrCreate($attributes, $values);
    }

    /**
     * {@inheritDoc}
     */
    public function getLengthMinMaxWord(): array
    {
        return Word::query()// @phpstan-ignore-line
            ->selectRaw('MIN(length) AS minLength, MAX(length) AS maxLength')
            ->get()
            ->toArray()[0];
    }

    /**
     * {@inheritDoc}
     */
    public function getToGenerateFromDictionaryRequest(
        DictionariesDTO $passwordsDictionariesRequest
    ): array {
        return Word::query()// @phpstan-ignore-line
            ->select(['prepared'])
            ->whereIn('lang_id', $passwordsDictionariesRequest->getDictionariesEnumIds())
            ->where('length', '>=', $passwordsDictionariesRequest->getLengthMin())
            ->where('length', '<=', $passwordsDictionariesRequest->getLengthMax())
            ->limit($passwordsDictionariesRequest->getPasswordsCount())
            ->inRandomOrder()
            ->pluck('prepared')
            ->toArray();
    }
}
