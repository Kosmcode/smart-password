<?php

namespace App\Repositories;

use App\Dtos\Request\Passwords\DictionariesDTO;
use App\Models\Word;

/**
 * Interface of Word Repository
 */
interface WordRepositoryInterface
{
    /**
     * Method to create or update Word
     */
    public function createOrUpdateWord(array $attributes, array $values): Word;

    /**
     * Method to get min and max length of all words
     */
    public function getLengthMinMaxWord(): array;

    /**
     * Method to get words to generate from dictionary request
     */
    public function getToGenerateFromDictionaryRequest(
        DictionariesDTO $passwordsDictionariesRequest
    ): array;
}
