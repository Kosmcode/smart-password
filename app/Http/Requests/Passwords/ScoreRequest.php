<?php

namespace App\Http\Requests\Passwords;

use App\Enums\RequestFieldEnum;
use Illuminate\Foundation\Http\FormRequest;
use Kosmcode\GoogleRecaptchaV3Rule\Rules\GoogleReCaptchaV3CheckRule;

/**
 * Password Score Request
 */
class ScoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            (string) RequestFieldEnum::password()->value => [
                'required',
                'string',
                'min:1',
            ],
            (string) RequestFieldEnum::captchaToken()->value => [
                'required',
                'string',
                new GoogleReCaptchaV3CheckRule(),
            ],
        ];
    }
}
