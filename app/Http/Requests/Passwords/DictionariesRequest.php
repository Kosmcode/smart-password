<?php

namespace App\Http\Requests\Passwords;

use App\Enums\RequestFieldEnum;
use App\Enums\WordLanguageEnum;
use App\Services\Word\LengthCacheServiceInterface;
use Illuminate\Config\Repository as Config;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Kosmcode\GoogleRecaptchaV3Rule\Rules\GoogleReCaptchaV3CheckRule;
use Psr\SimpleCache\InvalidArgumentException;

/**
 * Passwords Dictionaries Request
 */
class DictionariesRequest extends FormRequest
{
    public function __construct(
        private Config $config,
        private LengthCacheServiceInterface $wordLengthCacheGetService,
        array $query = [],
        array $request = [],
        array $attributes = [],
        array $cookies = [],
        array $files = [],
        array $server = [],
        $content = null
    ) {
        parent::__construct(
            $query,
            $request,
            $attributes,
            $cookies,
            $files,
            $server,
            $content
        );
    }

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     *
     * @throws InvalidArgumentException
     */
    public function rules(): array
    {
        $lengthMinMaxWord = $this->wordLengthCacheGetService->getCachedLengthMinMaxWords();

        return [
            (string) RequestFieldEnum::dictionaries()->value => [
                'required',
                'array',
                Rule::in(WordLanguageEnum::toLabels()),
            ],
            (string) RequestFieldEnum::camelCase()->value => [
                'required',
                'boolean',
            ],
            (string) RequestFieldEnum::lengthMin()->value => [
                'required',
                'integer',
                'between:'.$lengthMinMaxWord['minLength'].','.$lengthMinMaxWord['maxLength'],
            ],
            (string) RequestFieldEnum::lengthMax()->value => [
                'required',
                'integer',
                'between:'.$lengthMinMaxWord['minLength'].','.$lengthMinMaxWord['maxLength'],
                'gte:lengthMin',
            ],
            (string) RequestFieldEnum::passwordsCount()->value => [
                'required',
                Rule::in((array) $this->config->get('dictionary.form.passwordsCountAvailableValues')),
            ],
            (string) RequestFieldEnum::captchaToken()->value => [
                'required',
                'string',
                new GoogleReCaptchaV3CheckRule(),
            ],
        ];
    }
}
