<?php

namespace App\Http\Requests\Passwords;

use App\Enums\RequestFieldEnum;
use Illuminate\Config\Repository as Config;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Kosmcode\GoogleRecaptchaV3Rule\Rules\GoogleReCaptchaV3CheckRule;

class GenerateRandomRequest extends FormRequest
{
    public function __construct(
        private Config $config,
        array $query = [],
        array $request = [],
        array $attributes = [],
        array $cookies = [],
        array $files = [],
        array $server = [],
        $content = null
    ) {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
    }

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            (string) RequestFieldEnum::characters()->value => [
                'required',
                'array',
                'min:1',
                Rule::in(array_keys((array) $this->config->get('passwords.random.availableCharacters'))),
            ],
            (string) RequestFieldEnum::lengthMin()->value => [
                'required',
                'integer',
                'min:'.$this->config->get('passwords.random.length.min'),
                'max:'.$this->config->get('passwords.random.length.max'),
            ],
            (string) RequestFieldEnum::lengthMax()->value => [
                'required',
                'integer',
                'min:'.$this->config->get('passwords.random.length.min'),
                'max:'.$this->config->get('passwords.random.length.max'),
                'gte:lengthMin',
            ],
            (string) RequestFieldEnum::passwordsCount()->value => [
                'required',
                'integer',
                Rule::in((array) $this->config->get('passwords.random.countOptions.available')),
            ],
            (string) RequestFieldEnum::captchaToken()->value => [
                'required',
                'string',
                new GoogleReCaptchaV3CheckRule(),
            ],
        ];
    }
}
