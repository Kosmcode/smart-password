<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RouteFormRequest extends FormRequest
{
    public function validationData(): array
    {
        return array_merge($this->route()->parameters(), $this->all());
    }
}
