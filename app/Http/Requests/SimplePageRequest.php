<?php

namespace App\Http\Requests;

use App\Enums\LanguageIdCodeEnum;
use App\Providers\RouteServiceProvider;
use App\Rules\PageSlugRule;
use Illuminate\Validation\Rule;

class SimplePageRequest extends RouteFormRequest
{
    protected $redirect = RouteServiceProvider::HOME;

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'lang' => [
                'required',
                'string',
                Rule::in(LanguageIdCodeEnum::toLabels()),
                'bail',
            ],
            'slug' => [
                'required',
                'string',
                new PageSlugRule($this->route('lang')),
                'bail',
            ],
        ];
    }
}
