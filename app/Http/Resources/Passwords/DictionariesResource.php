<?php

namespace App\Http\Resources\Passwords;

use App\Dtos\Request\Passwords\DictionariesDTO;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property DictionariesDTO $resource
 */
class DictionariesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     */
    public function toArray($request): array
    {
        return $this->resource->getGeneratedPasswords();
    }
}
