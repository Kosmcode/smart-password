<?php

namespace App\Http\Resources\Passwords;

use App\Dtos\Request\Passwords\GenerateRandomDTO;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Passwords Random Generate Resource
 *
 * @property GenerateRandomDTO $resource
 */
class RandomGenerateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     */
    public function toArray($request): array
    {
        return [
            'generatedPasswords' => $this->resource->getGeneratedPasswords(),
        ];
    }
}
