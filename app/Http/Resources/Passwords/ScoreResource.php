<?php

namespace App\Http\Resources\Passwords;

use App\Dtos\Request\Passwords\ScoreDTO;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Password Score Resource
 *
 * @property ScoreDTO $resource
 */
class ScoreResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     */
    public function toArray($request): array
    {
        return [
            'score' => $this->resource->getScore(),
            'breakTime' => [
                'onlineThrottling100PerHour' => $this->resource->getOnlineThrottling100PerHour(),
                'onlineNoThrottling10PerSecond' => $this->resource->getOnlineNoThrottling10PerSecond(),
                'offlineSlowHashing10000PerSecond' => $this->resource->getOfflineSlowHashing10000PerSecond(),
                'offlineFastHashing10000000000PerSecond' => $this->resource->getOfflineFastHashing10000000000PerSecond(),
            ],
        ];
    }
}
