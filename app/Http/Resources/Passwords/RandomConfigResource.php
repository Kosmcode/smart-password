<?php

namespace App\Http\Resources\Passwords;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Password Random Config Resource
 *
 * @property array $resource
 */
class RandomConfigResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     */
    public function toArray($request): array
    {
        return [
            'availableCharacters' => $this->resource['availableCharacters'],
            'countOptions' => $this->resource['countOptions']['available'],
            'countOptionsDefault' => $this->resource['countOptions']['default'],
            'lengthMin' => $this->resource['length']['min'],
            'lengthMax' => $this->resource['length']['max'],
        ];
    }
}
