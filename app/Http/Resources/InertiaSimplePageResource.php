<?php

namespace App\Http\Resources;

use App\Dtos\Request\Page\InertiaSimplePageDTO;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property InertiaSimplePageDTO $resource
 */
class InertiaSimplePageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     */
    public function toArray($request): array
    {
        return [
            'content' => $this->resource->getContent(),
        ];
    }
}
