<?php

namespace App\Http\Resources\Dictionaries;

use App\Dtos\DictionariesConfigDto;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Dictionaries Config Resource
 *
 * @property DictionariesConfigDto $resource
 */
class ConfigResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     */
    public function toArray($request): array
    {
        return [
            'dictionaries' => $this->resource->getDictionaries(),
            'camelCase' => $this->resource->getCamelCase(),
            'lengthMin' => $this->resource->getLengthMin(),
            'lengthMax' => $this->resource->getLengthMax(),
            'passwordsCount' => [
                'default' => $this->resource->getPassCountDefault(),
                'options' => $this->resource->getPassCountOptions(),
            ],
        ];
    }
}
