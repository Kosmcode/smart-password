<?php

namespace App\Http\Middleware;

use App\Services\Locale\RedirectServiceInterface;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class LocaleRedirectMiddleware
{
    public function __construct(
        private RedirectServiceInterface $redirectService
    ) {

    }

    /**
     * Handle an incoming request.
     */
    public function handle(Request $request): Response|RedirectResponse
    {
        return $this->redirectService->redirectOnMiddleware($request);
    }
}
