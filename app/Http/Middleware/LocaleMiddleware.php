<?php

namespace App\Http\Middleware;

use App\Services\Locale\SetServiceInterface;
use Closure;
use Illuminate\Http\Request;

class LocaleMiddleware
{
    public function __construct(
        private SetServiceInterface $setService
    ) {
    }

    public function handle(Request $request, Closure $next)
    {
        $this->setService->setOnWebMiddleware($request);

        return $next($request);
    }
}
