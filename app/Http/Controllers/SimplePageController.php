<?php

namespace App\Http\Controllers;

use App\Dtos\Request\Page\InertiaSimplePageDTO;
use App\Enums\InertiaComponentEnum;
use App\Http\Requests\SimplePageRequest;
use App\Http\Resources\InertiaSimplePageResource;
use App\Services\Page\SimpleGetServiceInterface;
use Illuminate\Http\RedirectResponse;
use Inertia\Response;
use Inertia\ResponseFactory;

class SimplePageController extends Controller
{
    public function __construct(
        private ResponseFactory $responseFactory,
        private SimpleGetServiceInterface $simpleGetService
    ) {
    }

    public function getByLangAndSlug(SimplePageRequest $simplePageRequest): Response|RedirectResponse
    {
        return $this->responseFactory->render(
            InertiaComponentEnum::pageSimple()->value,
            InertiaSimplePageResource::make(
                $this->simpleGetService->getSimplePageByDTO(
                    new InertiaSimplePageDTO($simplePageRequest)
                )
            )->toArray($simplePageRequest)
        );
    }
}
