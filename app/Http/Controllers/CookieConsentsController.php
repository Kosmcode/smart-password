<?php

namespace App\Http\Controllers;

use App\Services\Cookie\Consents\UserServiceInterface;
use App\Services\User\SiteDataDeleteServiceInterface;
use Illuminate\Contracts\Routing\ResponseFactory as Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector as Redirect;

/**
 * Cookie Consents Controller
 */
class CookieConsentsController extends Controller
{
    public function __construct(
        private UserServiceInterface $cookiesConsentsUserService,
        private Redirect $redirect,
        private SiteDataDeleteServiceInterface $cookiesDeleteService,
        private Response $response
    ) {
    }

    public function accepted(): JsonResponse
    {
        $this->cookiesConsentsUserService->setAcceptCookies();

        return $this->response->json();
    }

    public function dontAccepted(): RedirectResponse
    {
        return $this->redirect->to('https://google.com/')
            ->withCookies($this->cookiesDeleteService->deleteAll());
    }
}
