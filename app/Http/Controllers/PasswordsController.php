<?php

namespace App\Http\Controllers;

use App\Dtos\Request\Passwords\DictionariesDTO;
use App\Dtos\Request\Passwords\GenerateRandomDTO;
use App\Dtos\Request\Passwords\ScoreDTO;
use App\Http\Requests\Passwords\DictionariesRequest;
use App\Http\Requests\Passwords\GenerateRandomRequest;
use App\Http\Requests\Passwords\ScoreRequest;
use App\Http\Resources\Passwords\DictionariesResource;
use App\Http\Resources\Passwords\RandomGenerateResource;
use App\Http\Resources\Passwords\ScoreResource;
use App\Services\Password\Dictionaries\GenerateServiceInterface;
use App\Services\Password\Random\GenerateServiceInterface as RandomGenerateService;
use App\Services\Password\ScoreServiceInterface;
use Exception;

/**
 * Passwords Controller
 */
class PasswordsController extends Controller
{
    public function __construct(
        private GenerateServiceInterface $passDictionariesGenerateSrv,
        private ScoreServiceInterface $passwordScoreService,
        private RandomGenerateService $passRandomGenerateSrv
    ) {
    }

    /**
     * @throws Exception
     */
    public function generateFromDictionaries(DictionariesRequest $passwordsDictionariesRequest): DictionariesResource
    {
        return DictionariesResource::make(
            $this->passDictionariesGenerateSrv->generateByDictionariesDTO(
                new DictionariesDTO($passwordsDictionariesRequest)
            )
        );
    }

    public function getPasswordScore(ScoreRequest $passwordScoreRequest): ScoreResource
    {
        return ScoreResource::make(
            $this->passwordScoreService->getScoreByScoreDTO(
                new ScoreDTO($passwordScoreRequest)
            )
        );
    }

    /**
     * @throws Exception
     */
    public function generateRandomPasswords(
        GenerateRandomRequest $passGenerateRandomRequest
    ): RandomGenerateResource {
        return RandomGenerateResource::make(
            $this->passRandomGenerateSrv->generateByGenerateRandomDTO(
                new GenerateRandomDTO($passGenerateRandomRequest)
            )
        );
    }
}
