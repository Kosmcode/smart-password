<?php

namespace App\Http\Controllers;

use App\Enums\InertiaComponentEnum;
use App\Http\Resources\Dictionaries\ConfigResource;
use App\Http\Resources\Passwords\RandomConfigResource;
use App\Services\Password\Dictionaries\ConfigGetServiceInterface;
use App\Services\Password\Random\ConfigCacheGetServiceInterface;
use Inertia\Response;
use Inertia\ResponseFactory;
use Psr\SimpleCache\InvalidArgumentException;

class MainController extends Controller
{
    public function __construct(
        private ResponseFactory $responseFactory,
        private ConfigGetServiceInterface $passDictionariesConfigGetSrv,
        private ConfigCacheGetServiceInterface $passRandomConfigCacheGetSrv,
    ) {
    }

    /**
     * @throws InvalidArgumentException
     */
    public function getMainPage(): Response
    {
        return $this->responseFactory
            ->render(
                InertiaComponentEnum::pageMain()->value,
                [
                    'config' => [
                        'dictionaries' => ConfigResource::make(
                            $this->passDictionariesConfigGetSrv->getFormDictionariesConfig()
                        ),
                        'random' => RandomConfigResource::make(
                            $this->passRandomConfigCacheGetSrv->getCachedConfigToRequest()
                        ),
                    ],
                ]
            );
    }
}
