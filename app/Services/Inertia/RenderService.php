<?php

namespace App\Services\Inertia;

use App\Enums\InertiaComponentEnum;
use App\Enums\ViewEnum;
use App\Services\Page\SimpleGetServiceInterface;
use Inertia\Response;
use Inertia\ResponseFactory as InertiaResponseFactory;

/**
 * Inertia Render Service
 */
class RenderService implements RenderServiceInterface
{
    public function __construct(
        protected InertiaResponseFactory $inertiaResponseFactory,
        protected SimpleGetServiceInterface $pageSimpleGetService
    ) {
    }

    /**
     * {@inheritDoc}
     */
    public function renderSimplePageByViewEnum(ViewEnum $viewEnum): Response
    {
        return $this->inertiaResponseFactory->render(
            (string) InertiaComponentEnum::pageSimple()->value,
            [
                self::SIMPLE_PAGE_VIEW_CONTENT_KEY => $this->pageSimpleGetService
                    ->getSimplePageByDTO((string) $viewEnum->value),
            ]
        );
    }
}
