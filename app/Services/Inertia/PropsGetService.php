<?php

namespace App\Services\Inertia;

use App\Services\Cookie\Consents\GetServiceInterface;
use App\Services\Language\AvaliablesGetServiceInterface;
//use Illuminate\Session\SessionManager;
use Illuminate\Translation\Translator as Lang;

/**
 * Inertia Props Get Service
 */
class PropsGetService implements PropsGetServiceInterface
{
    public function __construct(
        protected Lang $lang,
        protected AvaliablesGetServiceInterface $languageAvaliablesGetService,
        protected GetServiceInterface $cookiesConsentsGetService,
        //protected SessionManager $sessionManager,
    ) {
    }

    /**
     * {@inheritDoc}
     */
    public function getPropsToHandleMiddleware(): array
    {
        return [
            'locale' => [
                'current' => $this->lang->getLocale(),
                'fallback' => $this->lang->getFallback(),
            ],
            'languages' => $this->languageAvaliablesGetService->getToInertia(),
            'cookies' => $this->cookiesConsentsGetService->getCookiesConsentsConfigToProps(),
            //'csrf_token' => $this->sessionManager->token(),
        ];
    }
}
