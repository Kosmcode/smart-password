<?php

namespace App\Services\Inertia;

use App\Exceptions\LanguageException;

/**
 * Interface of Inertia Props Get Service
 */
interface PropsGetServiceInterface
{
    /**
     * Method to get props to handle inertia middleware
     *
     *
     * @throws LanguageException
     */
    public function getPropsToHandleMiddleware(): array;
}
