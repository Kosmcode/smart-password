<?php

namespace App\Services\Inertia;

use App\Enums\ViewEnum;
use Inertia\Response;

/**
 * Interface of Inertia Render Service
 */
interface RenderServiceInterface
{
    const SIMPLE_PAGE_VIEW_CONTENT_KEY = 'content';

    /**
     * Method to render Simple Page by View Enum
     */
    public function renderSimplePageByViewEnum(ViewEnum $viewEnum): Response;
}
