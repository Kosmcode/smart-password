<?php

namespace App\Services\Cookie\Consents;

use Illuminate\Routing\UrlGenerator as Url;

/**
 * Cookies Consents Get Service
 */
class GetService implements GetServiceInterface
{
    public function __construct(
        protected UserServiceInterface $cookiesConsentsUserService,
        protected Url $url
    ) {
    }

    /**
     * {@inheritDoc}
     */
    public function getCookiesConsentsConfigToProps(): array
    {
        return [
            'consents' => [
                'userAccepted' => $this->cookiesConsentsUserService->isAccepted(),
            ],
        ];
    }
}
