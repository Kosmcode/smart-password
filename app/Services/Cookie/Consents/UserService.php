<?php

namespace App\Services\Cookie\Consents;

use Illuminate\Cookie\CookieJar as Cookie;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Session\SessionManager as Session;

/**
 * Cookies Consents User Service
 */
class UserService implements UserServiceInterface
{
    public function __construct(
        protected Cookie $cookie,
        protected Session $session,
        protected Request $request,
        protected Response $response
    ) {
    }

    /**
     * {@inheritDoc}
     */
    public function isAccepted(): bool
    {
        $userAcceptedCookie = $this->session->get(self::COOKIE_CONSENTS_NAME);

        if (is_array($userAcceptedCookie) && isset($userAcceptedCookie[0]) && $userAcceptedCookie[0] === true) {
            return true;
        }

        $userAcceptedCookie = (bool) $this->request->cookie(self::COOKIE_CONSENTS_NAME);

        if ($userAcceptedCookie === true) {
            return true;
        }

        return false;
    }

    /**
     * {@inheritDoc}
     */
    public function setAcceptCookies(): void
    {
        $this->session->push(self::COOKIE_CONSENTS_NAME, true);

        $this->cookie->forever(self::COOKIE_CONSENTS_NAME, '1');
    }
}
