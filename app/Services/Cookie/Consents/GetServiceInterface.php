<?php

namespace App\Services\Cookie\Consents;

/**
 * Interface of Cookies Consents Get Service
 */
interface GetServiceInterface
{
    /**
     * Method to get Cookies Consents Config to Inertia props
     *
     * @return array[]
     */
    public function getCookiesConsentsConfigToProps(): array;
}
