<?php

namespace App\Services\Cookie\Consents;

/**
 * Interface of Cookies Consents User Service
 */
interface UserServiceInterface
{
    const COOKIE_CONSENTS_NAME = 'cookieConsentsAccepted';

    /**
     * Method to check if user accepted cookies consents
     */
    public function isAccepted(): bool;

    /**
     * Method to set cookies consent for user
     */
    public function setAcceptCookies(): void;
}
