<?php

namespace App\Services\Locale;

use App\Enums\LanguageIdCodeEnum;
use Illuminate\Config\Repository as Config;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector as Redirect;

/**
 * Locale Redirect Service
 */
class RedirectService implements RedirectServiceInterface
{
    public function __construct(
        protected Config $config,
        protected Redirect $redirect
    ) {
    }

    /**
     * {@inheritDoc}
     */
    public function redirectOnMiddleware(Request $request): RedirectResponse
    {
        $userLocale = $request->session()->get('locale');

        if (! empty($userLocale)) {
            return $this->redirect->route('main', ['lang' => $userLocale]);
        }

        $userLocale = $request->getPreferredLanguage(LanguageIdCodeEnum::toLabels())
            ?? $this->config->get('app.locale');

        return $this->redirect->route('main', ['lang' => $userLocale]);
    }
}
