<?php

namespace App\Services\Locale;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

/**
 * Interface of Locale Redirect Service
 */
interface RedirectServiceInterface
{
    /**
     * Method to redirect proper locale
     */
    public function redirectOnMiddleware(Request $request): RedirectResponse;
}
