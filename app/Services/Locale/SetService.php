<?php

namespace App\Services\Locale;

use App\Enums\LanguageIdCodeEnum;
use Illuminate\Config\Repository as Config;
use Illuminate\Http\Request;
use Illuminate\Translation\Translator as Lang;

/**
 * Locale Set Service
 */
class SetService implements SetServiceInterface
{
    public function __construct(
        protected Config $config,
        protected Lang $lang
    ) {
    }

    /**
     * {@inheritDoc}
     */
    public function setOnWebMiddleware(Request $request): void
    {
        $locale = $request->route('lang')
            ?? $request->session()->get('locale')
            ?? $request->getPreferredLanguage(LanguageIdCodeEnum::toLabels())
            ?? $this->config->get('app.locale');

        $request->session()->put('locale', $locale);

        $this->lang->setLocale($locale);
    }
}
