<?php

namespace App\Services\Locale;

use Illuminate\Http\Request;

/**
 * Interface of Locale Set Service
 */
interface SetServiceInterface
{
    /**
     * Method to set locale on middleware
     */
    public function setOnWebMiddleware(Request $request): void;
}
