<?php

namespace App\Services\Password;

use ZxcvbnPhp\Zxcvbn as PasswordStrengthRate;

/**
 * Password Strength Rate Service
 */
class StrengthRateService implements StrengthRateServiceInterface
{
    public function __construct(
        protected PasswordStrengthRate $passwordStrengthRate
    ) {
    }

    /**
     * {@inheritDoc}
     */
    public function ratePasswordForDictionaries(string $password): ?int
    {
        $ratePassword = $this->passwordStrengthRate->passwordStrength($password);

        if (! isset($ratePassword['score'])) {
            return null;
        }

        return $ratePassword['score'];
    }
}
