<?php

namespace App\Services\Password\Dictionaries;

use App\Dtos\Request\Passwords\DictionariesDTO;
use App\Exceptions\ConfigException;
use App\Repositories\WordRepositoryInterface;
use App\Services\Password\StrengthRateServiceInterface;
use Exception;
use Illuminate\Config\Repository as Config;

/**
 * Password Dictionaries Generate Service
 */
class GenerateService implements GenerateServiceInterface
{
    private int $countSpecialChar;

    public function __construct(
        protected WordRepositoryInterface $wordRepository,
        protected Config $config,
        protected StrengthRateServiceInterface $passwordStrengthRateService
    ) {
    }

    /**
     * {@inheritDoc}
     */
    public function generateByDictionariesDTO(DictionariesDTO $passwordsDictionariesRequest): DictionariesDTO
    {
        $result = $this->wordRepository->getToGenerateFromDictionaryRequest($passwordsDictionariesRequest);

        if ($result === []) {
            return $passwordsDictionariesRequest->setGeneratedPasswords([]);
        }

        $preparedWords = [];

        $specialChars = $this->config->get('words.charToReplace');

        if (! is_array($specialChars)) {
            throw new ConfigException(
                'words.charToReplace',
                ConfigException::MESSAGE_INVALID_VALUE
            );
        }

        $specialCharsKey = array_keys($specialChars);

        foreach ($result as $word) {
            $this->countSpecialChar = 0;

            $wordSplit = str_split($word);

            $preparedWord = '';

            foreach ($wordSplit as $index => $char) {
                if ($passwordsDictionariesRequest->getCamelCase() === true && $index % 2 === 0) {
                    $char = strtoupper($char);
                }

                $preparedWord .= $this->prepareSpecialChar($char, $specialChars, $specialCharsKey);
            }

            $preparedWords[] = [
                'password' => $preparedWord,
                'score' => $this->passwordStrengthRateService->ratePasswordForDictionaries($preparedWord),
            ];
        }

        return $passwordsDictionariesRequest->setGeneratedPasswords($preparedWords);
    }

    /**
     * Method to prepare special char in password
     *
     * @throws Exception
     */
    private function prepareSpecialChar(string $char, array $specialChars, array $specialCharsKey): string
    {
        if (in_array($char, $specialCharsKey)) {
            if ($this->countSpecialChar >= 2) {
                if (random_int(0, 50) > 0) {
                    return $char;
                }
            }

            $specialChar = $specialChars[$char];

            if (is_array($specialChar)) {
                $char = $specialChar[array_rand($specialChar)];
            }

            if (is_string($specialChar)) {
                $char = $specialChar;
            }

            $this->countSpecialChar++;
        }

        return $char;
    }
}
