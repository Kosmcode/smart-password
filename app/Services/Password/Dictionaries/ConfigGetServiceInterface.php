<?php

namespace App\Services\Password\Dictionaries;

use App\Dtos\DictionariesConfigDto;
use Psr\SimpleCache\InvalidArgumentException;

/**
 * Interface of Password Dictionaries Config Get Service
 */
interface ConfigGetServiceInterface
{
    /**
     * Method to get form dictionaries config
     *
     *
     * @throws InvalidArgumentException
     */
    public function getFormDictionariesConfig(): DictionariesConfigDto;
}
