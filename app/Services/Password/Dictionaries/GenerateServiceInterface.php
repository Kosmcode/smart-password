<?php

namespace App\Services\Password\Dictionaries;

use App\Dtos\Request\Passwords\DictionariesDTO;
use Exception;

/**
 * interface of Password Dictionaries Generate Service Interface
 */
interface GenerateServiceInterface
{
    /**
     * Method to generate password from dictionary request
     *
     * @throws Exception
     */
    public function generateByDictionariesDTO(DictionariesDTO $passwordsDictionariesRequest): DictionariesDTO;
}
