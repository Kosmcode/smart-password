<?php

namespace App\Services\Password\Dictionaries;

use App\Dtos\DictionariesConfigDto;
use App\Enums\LanguageIdCodeEnum;
use App\Enums\WordLanguageEnum;
use App\Exceptions\ConfigException;
use App\Services\Word\LengthCacheServiceInterface;
use Exception;
use Illuminate\Config\Repository as Config;
use Illuminate\Translation\Translator as Lang;

/**
 * Password Dictionaries Config Get Service
 */
class ConfigGetService implements ConfigGetServiceInterface
{
    public function __construct(
        protected LengthCacheServiceInterface $wordLengthCacheService,
        protected Config $config,
        protected Lang $lang
    ) {

    }

    /**
     * {@inheritDoc}
     *
     * @throws Exception
     */
    public function getFormDictionariesConfig(): DictionariesConfigDto
    {
        $lengthMinMaxWords = $this->wordLengthCacheService->getCachedLengthMinMaxWords();

        if (! is_int($this->config->get('dictionary.form.passwordsCountDefault'))) {
            throw new ConfigException(
                'dictionary.form.passwordsCountDefault',
                ConfigException::MESSAGE_INVALID_VALUE
            );
        }

        return (new DictionariesConfigDto())->setLengthMax($lengthMinMaxWords['maxLength'])->setLengthMin($lengthMinMaxWords['minLength'])->setCamelCase((bool) $this->config->get('dictionary.form.camelCaseChecked'))->setPassCountDefault($this->config->get('dictionary.form.passwordsCountDefault'))->setPassCountOptions((array) $this->config->get('dictionary.form.passwordsCountAvailableValues'))->setDictionaries($this->prepareDictionariesData());
    }

    /**
     * Method to prepare dictionaries data
     */
    private function prepareDictionariesData(): array
    {
        $dictionariesData = [];

        $currentLocale = $this->lang->getLocale();

        foreach (WordLanguageEnum::toArray() as $dictionaryId => $dictionary) {
            $langCode = LanguageIdCodeEnum::from($dictionaryId)->label;

            $dictionariesData[] = [
                'option' => $dictionary,
                'default' => $langCode === $currentLocale,
            ];
        }

        return $dictionariesData;

    }
}
