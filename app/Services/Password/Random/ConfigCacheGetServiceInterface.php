<?php

namespace App\Services\Password\Random;

use Psr\SimpleCache\InvalidArgumentException;

/**
 * Interface of Password Random Config Cache Get Service
 */
interface ConfigCacheGetServiceInterface
{
    /**
     * Method to get cached config passwords random for request
     *
     *
     * @throws InvalidArgumentException
     */
    public function getCachedConfigToRequest(): array;
}
