<?php

namespace App\Services\Password\Random;

use App\Exceptions\ConfigException;
use Illuminate\Config\Repository as Config;

/**
 * Password Random Config Service
 */
class ConfigGetService implements ConfigGetServiceInterface
{
    public function __construct(
        protected Config $config
    ) {
    }

    /**
     * {@inheritDoc}
     */
    public function getConfigToRequest(): array
    {
        $availableCharacters = $this->config->get('passwords.random.availableCharacters');

        if (! is_array($availableCharacters)) {
            throw new ConfigException(
                'passwords.random.availableCharacters',
                ConfigException::MESSAGE_INVALID_VALUE
            );
        }

        $availableCharactersConfig = [];

        foreach ($availableCharacters as $key => $value) {
            $availableCharactersConfig[] = [
                'label' => $key,
                'characters' => implode(' ', $value),
            ];
        }

        return [
            'availableCharacters' => $availableCharactersConfig,
            'countOptions' => [
                'available' => $this->config->get('passwords.random.countOptions.available'),
                'default' => $this->config->get('passwords.random.countOptions.default'),
            ],
            'length' => [
                'min' => $this->config->get('passwords.random.length.min'),
                'max' => $this->config->get('passwords.random.length.max'),
            ],
        ];
    }
}
