<?php

namespace App\Services\Password\Random;

use App\Dtos\Request\Passwords\GenerateRandomDTO;
use App\Exceptions\ConfigException;
use Illuminate\Config\Repository as Config;
use ZxcvbnPhp\Zxcvbn as PasswordStrengthRate;

/**
 * Passwords Random Generate Service
 */
class GenerateService implements GenerateServiceInterface
{
    public function __construct(
        protected PasswordStrengthRate $passwordStrengthRate,
        protected Config $config
    ) {
    }

    /**
     * {@inheritDoc}
     */
    public function generateByGenerateRandomDTO(
        GenerateRandomDTO $generateRandomDTO
    ): GenerateRandomDTO {
        $charactersToGeneratePassword = [];

        foreach ($generateRandomDTO->getCharacters() as $charactersLabel) {
            $availableCharacters = $this->config->get('passwords.random.availableCharacters.'.$charactersLabel);

            if (! is_array($availableCharacters)) {
                throw new ConfigException(
                    'passwords.random.availableCharacters.'.$charactersLabel,
                    ConfigException::MESSAGE_INVALID_VALUE
                );
            }

            $charactersToGeneratePassword = array_merge(
                $charactersToGeneratePassword,
                $availableCharacters
            );
        }

        $charactersCount = count($charactersToGeneratePassword);

        $generatedPasswords = [];

        for ($iterator = 0; $iterator < $generateRandomDTO->getPasswordsCount(); $iterator++) {
            shuffle($charactersToGeneratePassword);

            $length = random_int(
                $generateRandomDTO->getLengthMin(),
                ($generateRandomDTO->getLengthMax() >= $charactersCount)
                    ? $charactersCount
                    : $generateRandomDTO->getLengthMax()
            );

            $randomCharacters = (array) array_rand($charactersToGeneratePassword, $length);

            $generatedPassword = '';

            foreach ($randomCharacters as $keyId) {
                $generatedPassword .= $charactersToGeneratePassword[$keyId];
            }

            $score = $this->passwordStrengthRate->passwordStrength($generatedPassword);

            $generatedPasswords[] = [
                'password' => $generatedPassword,
                'score' => $score['score'] ?? null,
            ];
        }

        return $generateRandomDTO->setGeneratedPasswords($generatedPasswords);
    }
}
