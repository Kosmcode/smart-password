<?php

namespace App\Services\Password\Random;

use App\Exceptions\ConfigException;

/**
 * Interface of Password Random Config Service
 */
interface ConfigGetServiceInterface
{
    /**
     * Method to get config to random password request
     *
     *
     * @throws ConfigException
     */
    public function getConfigToRequest(): array;
}
