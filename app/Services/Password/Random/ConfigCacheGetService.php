<?php

namespace App\Services\Password\Random;

use App\Enums\CacheKeyEnum;
use Illuminate\Cache\Repository as Cache;

/**
 * Password Random Config Cache Get Service
 */
class ConfigCacheGetService implements ConfigCacheGetServiceInterface
{
    public function __construct(
        protected ConfigGetServiceInterface $passwordRandomConfigGetService,
        protected Cache $cache
    ) {
    }

    /**
     * {@inheritDoc}
     */
    public function getCachedConfigToRequest(): array
    {
        $config = $this->cache->get((string) CacheKeyEnum::passwordsRandomConfig()->value);

        if (! is_array($config)) {
            $config = $this->passwordRandomConfigGetService->getConfigToRequest();

            $this->cache->set((string) CacheKeyEnum::passwordsRandomConfig()->value, $config);
        }

        return $config;
    }
}
