<?php

namespace App\Services\Password\Random;

use App\Dtos\Request\Passwords\GenerateRandomDTO;
use Exception;

/**
 * Interface of Passwords Random Generate Service
 */
interface GenerateServiceInterface
{
    /**
     * Method to generate random password
     *
     * @throws Exception
     */
    public function generateByGenerateRandomDTO(
        GenerateRandomDTO $generateRandomDTO
    ): GenerateRandomDTO;
}
