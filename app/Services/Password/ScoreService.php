<?php

namespace App\Services\Password;

use App\Dtos\Request\Passwords\ScoreDTO;
use ZxcvbnPhp\Zxcvbn as PasswordStrengthRate;

/**
 * Password Score Service
 */
class ScoreService implements ScoreServiceInterface
{
    public function __construct(
        protected PasswordStrengthRate $passwordStrengthRate,
    ) {
    }

    /**
     * {@inheritDoc}
     */
    public function getScoreByScoreDTO(ScoreDTO $scoreDTO): ScoreDTO
    {
        $result = $this->passwordStrengthRate->passwordStrength(
            $scoreDTO->getPassword()
        );

        return $scoreDTO
            ->setScore($result['score'])
            ->setOnlineThrottling100PerHour($result['crack_times_seconds']['online_throttling_100_per_hour'])
            ->setOnlineNoThrottling10PerSecond($result['crack_times_seconds']['online_no_throttling_10_per_second'])
            ->setOfflineSlowHashing10000PerSecond($result['crack_times_seconds']['offline_slow_hashing_1e4_per_second'])
            ->setOfflineFastHashing10000000000PerSecond($result['crack_times_seconds']['offline_fast_hashing_1e10_per_second']);
    }
}
