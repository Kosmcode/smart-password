<?php

namespace App\Services\Password;

use App\Dtos\Request\Passwords\ScoreDTO;

/**
 * Interface of Password Score Service
 */
interface ScoreServiceInterface
{
    /**
     * Method to get score password for request
     */
    public function getScoreByScoreDTO(ScoreDTO $scoreDTO): ScoreDTO;
}
