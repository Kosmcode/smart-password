<?php

namespace App\Services\Password;

/**
 * Interface of Password Strength Rate Service
 */
interface StrengthRateServiceInterface
{
    /**
     * Method to rate score password for dictionaries
     */
    public function ratePasswordForDictionaries(string $password): ?int;
}
