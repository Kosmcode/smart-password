<?php

namespace App\Services\Word;

use App\Models\Word;

/**
 * Interface of Add Word Service
 */
interface AddServiceInterface
{
    /**
     * Method to add word to db
     */
    public function addWordToDb(string $originalWord): ?Word;

    public function setMinExpectedSpecChars(int $minExpectedSpecChars): void;

    public function setLangId(int $langId): void;

    public function setExpectedCharsPattern(string $expectedCharsPattern): void;
}
