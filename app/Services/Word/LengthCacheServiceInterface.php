<?php

namespace App\Services\Word;

use Psr\SimpleCache\InvalidArgumentException;

/**
 * Interface of Word Length Cache Service
 */
interface LengthCacheServiceInterface
{
    const CACHE_KEY_LENGTH_MIN_MAX_WORD = 'length_min_max_word';

    /**
     * Method to get cached length min and max of words
     *
     *
     * @throws InvalidArgumentException
     */
    public function getCachedLengthMinMaxWords(): array;
}
