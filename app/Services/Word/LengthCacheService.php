<?php

namespace App\Services\Word;

use App\Repositories\WordRepositoryInterface;
use Illuminate\Cache\CacheManager as Cache;

/**
 * Word Length Cache Service
 */
class LengthCacheService implements LengthCacheServiceInterface
{
    public function __construct(
        protected WordRepositoryInterface $wordRepository,
        protected Cache $cache
    ) {
    }

    /**
     * {@inheritDoc}
     */
    public function getCachedLengthMinMaxWords(): array
    {
        $lengthMinMaxWord = $this->cache->get(self::CACHE_KEY_LENGTH_MIN_MAX_WORD);

        if (is_array($lengthMinMaxWord)) {
            return $lengthMinMaxWord;
        }

        $lengthMinMaxWord = $this->wordRepository->getLengthMinMaxWord();

        $this->cache->set(self::CACHE_KEY_LENGTH_MIN_MAX_WORD, $lengthMinMaxWord);

        return $lengthMinMaxWord;
    }
}
