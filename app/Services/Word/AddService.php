<?php

namespace App\Services\Word;

use App\Models\Word;
use App\Repositories\WordRepositoryInterface;

/**
 * Add Word Service
 */
class AddService implements AddServiceInterface
{
    private int $minExpectedSpecChars;

    private int $langId;

    private string $expectedCharsPattern;

    public function __construct(
        protected WordRepositoryInterface $wordRepository,
    ) {
    }

    /**
     * {@inheritDoc}
     */
    public function addWordToDb(string $originalWord): ?Word
    {
        $preparedWord = strtolower((string) iconv('UTF-8', 'ASCII//TRANSLIT', $originalWord));

        $countSpecChars = preg_match_all($this->expectedCharsPattern, $preparedWord, $matches);

        if ($countSpecChars < $this->minExpectedSpecChars) {
            return null;
        }

        $availableSpecChars = implode(',', array_unique($matches[0]));

        return $this->wordRepository->createOrUpdateWord(
            [
                'lang_id' => $this->langId,
                'original' => $originalWord,
            ],
            [
                'prepared' => $preparedWord,
                'length' => strlen($preparedWord),
                'count_special_chars' => $countSpecChars,
                'available_spec_chars' => $availableSpecChars,
            ]
        );
    }

    /**
     * {@inheritDoc}
     */
    public function setMinExpectedSpecChars(int $minExpectedSpecChars): void
    {
        $this->minExpectedSpecChars = $minExpectedSpecChars;
    }

    /**
     * {@inheritDoc}
     */
    public function setLangId(int $langId): void
    {
        $this->langId = $langId;
    }

    /**
     * {@inheritDoc}
     */
    public function setExpectedCharsPattern(string $expectedCharsPattern): void
    {
        $this->expectedCharsPattern = $expectedCharsPattern;
    }
}
