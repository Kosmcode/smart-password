<?php

namespace App\Services\User;

use Illuminate\Cookie\CookieJar as Cookie;
use Illuminate\Http\Request;
use Illuminate\Session\SessionManager as Session;

/**
 * User Site Data Delete Service
 */
class SiteDataDeleteService implements SiteDataDeleteServiceInterface
{
    public function __construct(
        protected Cookie $cookie,
        protected Session $session,
        protected Request $request
    ) {
    }

    /**
     * {@inheritDoc}
     */
    public function deleteAll(): array
    {
        $cookies = [];

        foreach (array_keys($this->request->cookie()) as $cookieName) { // @phpstan-ignore-line
            $cookies[] = $this->cookie->forget((string) $cookieName);
        }

        $this->session->flush();

        return $cookies;
    }
}
