<?php

namespace App\Services\User;

use Symfony\Component\HttpFoundation\Cookie;

/**
 * Interface of User Site Data Delete Service
 */
interface SiteDataDeleteServiceInterface
{
    /**
     * Method to delete all user cookies
     *
     * @return array<Cookie>
     */
    public function deleteAll(): array;
}
