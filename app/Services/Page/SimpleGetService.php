<?php

namespace App\Services\Page;

use App\Dtos\Request\Page\InertiaSimplePageDTO;
use Illuminate\View\Factory as ViewFactory;

/**
 * Page Simple Get Service
 */
class SimpleGetService implements SimpleGetServiceInterface
{
    public function __construct(
        protected ViewFactory $viewFactory
    ) {
    }

    /**
     * {@inheritDoc}
     */
    public function getSimplePageByDTO(InertiaSimplePageDTO $inertiaSimplePageDTO): InertiaSimplePageDTO
    {
        return $inertiaSimplePageDTO->setContent(
            $this->viewFactory
                ->make($inertiaSimplePageDTO->getViewName())
                ->render()
        );
    }
}
