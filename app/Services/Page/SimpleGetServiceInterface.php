<?php

namespace App\Services\Page;

use App\Dtos\Request\Page\InertiaSimplePageDTO;

/**
 * Interface Page Simple Get Service
 */
interface SimpleGetServiceInterface
{
    /**
     * Method to get simple page view by view name
     */
    public function getSimplePageByDTO(InertiaSimplePageDTO $inertiaSimplePageDTO): InertiaSimplePageDTO;
}
