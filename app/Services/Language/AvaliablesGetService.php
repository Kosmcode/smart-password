<?php

namespace App\Services\Language;

use App\Exceptions\LanguageException;
use Illuminate\Config\Repository as Config;

/**
 * Language Avaliables Service
 */
class AvaliablesGetService implements AvaliablesGetServiceInterface
{
    public function __construct(
        protected Config $config
    ) {
    }

    /**
     * {@inheritDoc}
     */
    public function getToInertia(): array
    {
        $languages = $this->config->get('languages.avaliables');

        if (empty($languages) || ! is_array($languages)) {
            throw new LanguageException('Not set avaliables language config.');
        }

        return array_values($languages);
    }
}
