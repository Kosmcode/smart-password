<?php

namespace App\Services\Language;

use App\Exceptions\LanguageException;

/**
 * Interface of Language Avaliables Service
 */
interface AvaliablesGetServiceInterface
{
    /**
     * Method to get to inertia props
     *
     *
     * @throws LanguageException
     */
    public function getToInertia(): array;
}
