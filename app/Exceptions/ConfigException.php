<?php

namespace App\Exceptions;

use Exception;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class ConfigException extends Exception
{
    public const MESSAGE_INVALID_VALUE = 'Invalid value';

    public function __construct(
        string $configKey,
        ?string $messageError = null,
        ?Throwable $previous = null
    ) {
        $message = 'Config exception - `'.$configKey.'`';

        if ($messageError !== null) {
            $message .= ' '.$messageError;
        }

        parent::__construct(
            $message,
            Response::HTTP_INTERNAL_SERVER_ERROR,
            $previous
        );
    }
}
