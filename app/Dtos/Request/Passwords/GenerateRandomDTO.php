<?php

namespace App\Dtos\Request\Passwords;

use App\Dtos\Request\RequestDTO;

/**
 * @method array getCharacters()
 * @method int getLengthMax()
 * @method int getLengthMin()
 * @method int getPasswordsCount()
 * @method array getGeneratedPasswords()
 * @method self setGeneratedPasswords(array $generatedPasswords)
 */
class GenerateRandomDTO extends RequestDTO
{
}
