<?php

namespace App\Dtos\Request\Passwords;

use App\Dtos\Request\RequestDTO;
use App\Enums\WordLanguageEnum;

/**
 * @method int getLengthMin()
 * @method int getLengthMax()
 * @method int getPasswordsCount()
 * @method bool getCamelCase()
 * @method array getDictionaries()
 * @method array getGeneratedPasswords()
 * @method self setGeneratedPasswords(array $generatedPasswords)
 */
class DictionariesDTO extends RequestDTO
{
    public function getDictionariesEnumIds(): array
    {
        $dictionaryEnumIds = [];

        foreach ($this->getDictionaries() as $dictionary) {
            $dictionaryEnumIds[] = WordLanguageEnum::from($dictionary)->value;
        }

        return $dictionaryEnumIds;
    }
}
