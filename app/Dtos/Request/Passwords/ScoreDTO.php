<?php

namespace App\Dtos\Request\Passwords;

use App\Dtos\Request\RequestDTO;

/**
 * @method string getPassword()
 * @method null|int getScore()
 * @method self setScore(null|int $score)
 * @method null|float getOnlineThrottling100PerHour()
 * @method self setOnlineThrottling100PerHour(null|float $value)
 * @method null|float getOnlineNoThrottling10PerSecond()
 * @method self setOnlineNoThrottling10PerSecond(null|float $score)
 * @method null|float getOfflineSlowHashing10000PerSecond()
 * @method self setOfflineSlowHashing10000PerSecond(null|float $score)
 * @method null|float getOfflineFastHashing10000000000PerSecond()
 * @method self setOfflineFastHashing10000000000PerSecond(null|float $score)
 */
class ScoreDTO extends RequestDTO
{
}
