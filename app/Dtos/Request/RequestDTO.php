<?php

namespace App\Dtos\Request;

use Illuminate\Foundation\Http\FormRequest;
use Kosmcode\SimpleDto\AbstractDTO;
use Kosmcode\SimpleDto\Exception\SimpleDTOException;

class RequestDTO extends AbstractDTO
{
    protected FormRequest $formRequest;

    public function __construct(FormRequest $formRequest)
    {
        $this->formRequest = $formRequest;
    }

    /**
     * @throws SimpleDTOException
     */
    protected function getFieldValue(string $methodName): mixed
    {
        $fieldName = $this->getFieldNameFromMethod($methodName, 'get');

        if (isset($this->{$fieldName})) {
            return $this->{$fieldName};
        }

        $requestData = $this->formRequest->get($fieldName) ??
            $this->formRequest->route($fieldName);

        if ($requestData !== null) {
            return $requestData;
        }

        throw new SimpleDTOException('Not exist field `'.$fieldName.'`');
    }
}
