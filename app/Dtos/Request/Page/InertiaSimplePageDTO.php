<?php

namespace App\Dtos\Request\Page;

use App\Dtos\Request\RequestDTO;

/**
 * @method string getLang()
 * @method self setLang(string $lang)
 * @method string getSlug()
 * @method self setSlug(string $slug)
 * @method string getContent()
 * @method self setContent(string $content)
 */
class InertiaSimplePageDTO extends RequestDTO
{
    public function getViewName(): string
    {
        return $this->getLang().'.page.'.$this->getSlug();
    }
}
