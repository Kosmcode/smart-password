<?php

namespace App\Dtos;

use Kosmcode\SimpleDto\AbstractDTO;

/**
 * Dictionaries Config Dto
 *
 * @method array getDictionaries()
 * @method self setDictionaries(array $dictionaries)
 * @method bool getCamelCase()
 * @method self setCamelCase(bool $camelCase)
 * @method int getLengthMin()
 * @method self setLengthMin(int $lengthMin)
 * @method int getLengthMax()
 * @method self setLengthMax(int $lengthMax)
 * @method int getPassCountDefault()
 * @method self setPassCountDefault(int $passCountDefault)
 * @method array getPassCountOptions()
 * @method self setPassCountOptions(array $passCountOptions)
 */
class DictionariesConfigDto extends AbstractDTO
{
}
