<?php

namespace App\Providers;

use App\Repositories\WordRepository;
use App\Repositories\WordRepositoryInterface;
use App\Services\Cookie\Consents\GetService;
use App\Services\Cookie\Consents\GetServiceInterface;
use App\Services\Cookie\Consents\UserService;
use App\Services\Cookie\Consents\UserServiceInterface;
use App\Services\Inertia\PropsGetService;
use App\Services\Inertia\PropsGetServiceInterface;
use App\Services\Inertia\RenderService;
use App\Services\Inertia\RenderServiceInterface;
use App\Services\Language\AvaliablesGetService;
use App\Services\Language\AvaliablesGetServiceInterface;
use App\Services\Locale\RedirectService;
use App\Services\Locale\RedirectServiceInterface;
use App\Services\Locale\SetService;
use App\Services\Locale\SetServiceInterface;
use App\Services\Page\SimpleGetService;
use App\Services\Page\SimpleGetServiceInterface;
use App\Services\Password\Dictionaries\ConfigGetService as DictionariesConfigGetService;
use App\Services\Password\Dictionaries\ConfigGetServiceInterface as DictionariesConfigGetServiceInterface;
use App\Services\Password\Dictionaries\GenerateService as DictionariesGenerateService;
use App\Services\Password\Dictionaries\GenerateServiceInterface as DictionariesGenerateServiceInterface;
use App\Services\Password\Random\ConfigCacheGetService;
use App\Services\Password\Random\ConfigCacheGetServiceInterface;
use App\Services\Password\Random\ConfigGetService;
use App\Services\Password\Random\ConfigGetServiceInterface;
use App\Services\Password\Random\GenerateService;
use App\Services\Password\Random\GenerateServiceInterface;
use App\Services\Password\ScoreService;
use App\Services\Password\ScoreServiceInterface;
use App\Services\Password\StrengthRateService;
use App\Services\Password\StrengthRateServiceInterface;
use App\Services\User\SiteDataDeleteService;
use App\Services\User\SiteDataDeleteServiceInterface;
use App\Services\Word\AddService;
use App\Services\Word\AddServiceInterface;
use App\Services\Word\LengthCacheService;
use App\Services\Word\LengthCacheServiceInterface;
use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\ServiceProvider;
use URL;

class AppServiceProvider extends ServiceProvider
{
    /**
     * @var array|string[]
     */
    public array $bindings = [
        WordRepositoryInterface::class => WordRepository::class,
        AddServiceInterface::class => AddService::class,
        LengthCacheServiceInterface::class => LengthCacheService::class,
        DictionariesConfigGetServiceInterface::class => DictionariesConfigGetService::class,
        DictionariesGenerateServiceInterface::class => DictionariesGenerateService::class,
        StrengthRateServiceInterface::class => StrengthRateService::class,
        ScoreServiceInterface::class => ScoreService::class,
        ConfigGetServiceInterface::class => ConfigGetService::class,
        GenerateServiceInterface::class => GenerateService::class,
        AvaliablesGetServiceInterface::class => AvaliablesGetService::class,
        ConfigCacheGetServiceInterface::class => ConfigCacheGetService::class,
        SimpleGetServiceInterface::class => SimpleGetService::class,
        PropsGetServiceInterface::class => PropsGetService::class,
        GetServiceInterface::class => GetService::class,
        UserServiceInterface::class => UserService::class,
        SiteDataDeleteServiceInterface::class => SiteDataDeleteService::class,
        RenderServiceInterface::class => RenderService::class,
        SetServiceInterface::class => SetService::class,
        RedirectServiceInterface::class => RedirectService::class,
    ];

    /**
     * Register any application services.
     */
    public function register(): void
    {
        if ($this->app->isLocal()) {
            $this->app->register(IdeHelperServiceProvider::class);
        }
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        if (config('app.env') === 'production') {
            URL::forceScheme('https');
        }

        JsonResource::withoutWrapping();
    }
}
