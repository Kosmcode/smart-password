import './bootstrap';
import '../css/app.css';
import '../css/bootstrap.min.css';
import 'bootstrap/dist/css/bootstrap.css'

import {createI18n} from "vue-i18n";
import localeMessages from "./vue-i18n-locales.generated";
import {createApp, h} from 'vue';
import {createInertiaApp} from '@inertiajs/inertia-vue3';
import {InertiaProgress} from '@inertiajs/progress';
import {resolvePageComponent} from 'laravel-vite-plugin/inertia-helpers';
import {ZiggyVue} from '../../vendor/tightenco/ziggy/dist/vue.m';
import FlagIcon from 'vue-flag-icon'
import {BootstrapVue3} from 'bootstrap-vue-3'
import {VueReCaptcha} from 'vue-recaptcha-v3'
import { Inertia } from "@inertiajs/inertia";
import ScriptX from 'vue-scriptx'
import Ads from 'vue-google-adsense'

createInertiaApp({
    resolve: (name) => resolvePageComponent(`./Pages/${name}.vue`, import.meta.glob('./Pages/**/*.vue')),
    setup({el, app, props, plugin}) {
        const i18n = createI18n({
            locale: props.initialPage.props.locale.current,
            fallbackLocale: props.initialPage.props.locale.fallback,
            messages: localeMessages,
        });

        return createApp({render: () => h(app, props)})
            .use(plugin)
            .use(i18n)
            .use(ZiggyVue, Ziggy)
            .use(FlagIcon)
            .use(BootstrapVue3)
            .use(VueReCaptcha,
                {
                    siteKey: import.meta.env.VITE_GOOGLE_RECAPCHA_V3_SITE_KEY,
                    loaderOptions: {
                        autoHideBadge: true,
                    }
                })
            .use(ScriptX)
            .use(Ads.Adsense)
            .use(Ads.InArticleAdsense)
            .use(Ads.InFeedAdsense)
            .mount(el);
    },
});

InertiaProgress.init({color: '#4B5563'});

Inertia.on("navigate", (event) => {
    gtag("js", new Date());
    gtag("config", import.meta.env.VITE_GOOGLE_ANALITICS_GA4);
});
