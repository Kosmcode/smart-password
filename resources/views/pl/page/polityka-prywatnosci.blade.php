<article>
    <h1>Polityka prywatności</h1>
    <p>Ostatnia aktualizacja: 26 marca 2023 r</p>
    <h1>Interpretacja i definicje</h1>
    <h2>Interpretacja</h2>
    <p>Słowa, których pierwsza litera jest pisana wielką literą, mają znaczenie określone w następujących warunkach.
        Poniższe definicje mają takie samo znaczenie niezależnie od tego, czy występują w liczbie pojedynczej, czy
        mnogiej.</p>
    <h2>Definicje</h2>
    <p>Na potrzeby niniejszej Polityki prywatności:</p>
    <ul>
        <li><strong>Konto</strong> oznacza unikalne konto utworzone w celu uzyskania dostępu do naszej Usługi lub części
            naszej Usługi.
        </li>
        <li><strong>Podmiot stowarzyszony</strong> oznacza podmiot, który kontroluje, jest kontrolowany lub znajduje się
            pod wspólną kontrolą ze stroną, gdzie „kontrola” oznacza posiadanie co najmniej 50% udziałów, udziałów
            kapitałowych lub innych papierów wartościowych uprawniających do głosowania w wyborach dyrektorów lub innych
            organów zarządzających .
        </li>
        <li><strong>Firma</strong> (zwana dalej „Firmą”, „My”, „Nas” lub „Nasz” w niniejszej Umowie) odnosi się do
            KosmCODE Maksym Kawelski, Małopolska 49/14, 70-514 Szczecin.
        </li>
        <li><strong>Pliki cookie</strong> to małe pliki, które są umieszczane na Twoim komputerze, urządzeniu mobilnym
            lub innym urządzeniu przez witrynę internetową i zawierają między innymi szczegóły Twojej historii
            przeglądania tej witryny.
        </li>
        <li><strong>Kraj</strong> odnosi się do: Polska</li>
        <li><strong>Urządzenie</strong> oznacza dowolne urządzenie, które może uzyskać dostęp do Usługi, takie jak
            komputer, telefon komórkowy lub tablet cyfrowy.
        </li>
        <li><strong>Dane osobowe</strong> to wszelkie informacje dotyczące zidentyfikowanej lub możliwej do
            zidentyfikowania osoby fizycznej.
        </li>
        <li><strong>Usługa</strong> odnosi się do Serwisu.</li>
        <li><strong>Usługodawca</strong> oznacza każdą osobę fizyczną lub prawną, która przetwarza dane w imieniu
            Spółki. Odnosi się do firm zewnętrznych lub osób zatrudnionych przez Firmę w celu ułatwienia Usługi,
            świadczenia Usługi w imieniu Firmy, świadczenia usług związanych z Usługą lub pomocy Firmie w analizie
            sposobu korzystania z Usługi.
        </li>
        <li><strong>Dane dotyczące użytkowania</strong> odnoszą się do danych zbieranych automatycznie, generowanych
            przez korzystanie z Usługi lub z samej infrastruktury Usługi (na przykład czas trwania wizyty na stronie).
        </li>
        <li><strong>Witryna</strong> odnosi się do Inteligentnego hasła, dostępnego pod adresem <a
                href='https://www.smart-password.net/' target='_blank' class='url'>https://www.smart-password.net/</a>
        </li>
        <li><strong>Użytkownik</strong> oznacza odpowiednio osobę uzyskującą dostęp do Usługi lub korzystającą z niej,
            firmę lub inny podmiot prawny, w imieniu którego taka osoba uzyskuje dostęp do Usługi lub z niej korzysta.
        </li>

    </ul>
    <h1>Gromadzenie i wykorzystywanie Twoich danych osobowych</h1>
    <h2>Rodzaje zbieranych danych</h2>
    <h3>Dane osobiste</h3>
    <p>Podczas korzystania z naszej Usługi możemy poprosić Cię o podanie nam pewnych danych osobowych, które mogą być
        wykorzystane do skontaktowania się z Tobą lub zidentyfikowania Ciebie. Dane osobowe mogą obejmować między
        innymi:</p>
    <ul>
        <li>Adres e-mail</li>
        <li>Dane dotyczące użytkowania</li>

    </ul>
    <h3>Dane dotyczące użytkowania</h3>
    <p>Dane dotyczące użytkowania są zbierane automatycznie podczas korzystania z Usługi.</p>
    <p>Dane o użytkowaniu mogą obejmować informacje, takie jak adres protokołu internetowego Twojego Urządzenia (np.
        adres IP), typ przeglądarki, wersja przeglądarki, odwiedzane przez Ciebie strony naszego Serwisu, czas i data
        Twojej wizyty, czas spędzony na tych stronach, unikalne urządzenie identyfikatory i inne dane diagnostyczne.</p>
    <p>Kiedy uzyskujesz dostęp do Usługi za pośrednictwem urządzenia mobilnego, możemy automatycznie gromadzić pewne
        informacje, w tym między innymi rodzaj używanego urządzenia mobilnego, unikalny identyfikator urządzenia
        mobilnego, adres IP urządzenia mobilnego, numer telefonu system operacyjny, typ mobilnej przeglądarki
        internetowej, z której korzystasz, unikalne identyfikatory urządzenia i inne dane diagnostyczne.</p>
    <p>Możemy również gromadzić informacje, które Twoja przeglądarka wysyła za każdym razem, gdy odwiedzasz naszą Usługę
        lub gdy uzyskujesz dostęp do Usługi za pośrednictwem urządzenia mobilnego.</p>
    <h3>Technologie śledzenia i pliki cookie</h3>
    <p>Używamy plików cookie i podobnych technologii śledzenia, aby śledzić aktywność w naszym Serwisie i przechowywać
        określone informacje. Stosowane technologie śledzenia to sygnały nawigacyjne, tagi i skrypty do zbierania i
        śledzenia informacji oraz do ulepszania i analizowania naszej Usługi. Stosowane przez nas technologie mogą
        obejmować:</p>
    <ul>
        <li><strong>Pliki cookie lub pliki cookie przeglądarki.</strong> Cookie to mały plik umieszczany na Twoim
            Urządzeniu. Możesz poinstruować swoją przeglądarkę, aby odrzucała wszystkie pliki cookie lub wskazywała,
            kiedy plik cookie jest wysyłany. Jeśli jednak nie zaakceptujesz plików cookie, możesz nie być w stanie
            korzystać z niektórych części naszego Serwisu. O ile nie zmieniłeś ustawień swojej przeglądarki tak, aby
            odrzucała pliki cookie, nasz Serwis może wykorzystywać pliki cookie.
        </li>
        <li><strong>Sygnały nawigacyjne w sieci Web.</strong> Niektóre sekcje naszej Usługi i nasze wiadomości e-mail
            mogą zawierać małe pliki elektroniczne znane jako sygnały nawigacyjne (nazywane również czystymi gifami,
            znacznikami pikselowymi i jednopikselowymi gifami), które umożliwiają Spółce na przykład zliczanie
            użytkowników, którzy odwiedzili te strony lub otworzył wiadomość e-mail i inne powiązane statystyki witryny
            (na przykład rejestrowanie popularności określonej sekcji i sprawdzanie integralności systemu i serwera).
        </li>

    </ul>
    <p>Pliki cookie mogą być „stałe” lub „sesyjne”. Trwałe pliki cookie pozostają na Twoim komputerze osobistym lub
        urządzeniu mobilnym, gdy przechodzisz do trybu offline, podczas gdy sesyjne pliki cookie są usuwane, gdy tylko
        zamkniesz przeglądarkę internetową. Dowiedz się więcej o plikach cookie w artykule <a
            href='https://www.freeprivacypolicy.com/blog/sample-privacy-policy-template/#Use_Of_Cookies_And_Tracking'>dotyczącym
            bezpłatnej polityki prywatności</a> .</p>
    <p>Używamy zarówno sesyjnych, jak i trwałych plików cookie do celów określonych poniżej:</p>
    <ul>
        <li><p><strong>Niezbędne/niezbędne pliki cookie</strong></p>
            <p>Rodzaj: Pliki cookie sesji</p>
            <p>Administrowany przez: nas</p>
            <p>Cel: Te pliki cookie są niezbędne do świadczenia usług dostępnych za pośrednictwem Serwisu oraz
                umożliwienia korzystania z niektórych jego funkcji. Pomagają uwierzytelniać użytkowników i zapobiegać
                nieuczciwemu korzystaniu z kont użytkowników. Bez tych plików cookie usługi, o które prosiłeś, nie mogą
                być świadczone, a my używamy tych plików cookie wyłącznie do świadczenia tych usług.</p>
        </li>
        <li><p><strong>Polityka plików cookie / Informacja Akceptacja plików cookie</strong></p>
            <p>Typ: trwałe pliki cookie</p>
            <p>Administrowany przez: nas</p>
            <p>Cel: Te pliki cookie identyfikują, czy użytkownicy zaakceptowali użycie plików cookie w Witrynie.</p>
        </li>
        <li><p><strong>Funkcjonalne pliki cookie</strong></p>
            <p>Typ: trwałe pliki cookie</p>
            <p>Administrowany przez: nas</p>
            <p>Cel: Te pliki cookie pozwalają nam zapamiętać wybory, których dokonujesz podczas korzystania z Witryny,
                takie jak zapamiętywanie danych logowania lub preferencji językowych. Celem tych plików cookie jest
                zapewnienie bardziej osobistych doświadczeń i uniknięcie konieczności ponownego wprowadzania preferencji
                za każdym razem, gdy korzystasz z Witryny.</p>
        </li>

    </ul>
    <p>Aby uzyskać więcej informacji na temat używanych przez nas plików cookie i wyborów dotyczących plików cookie,
        odwiedź naszą Politykę plików cookie lub sekcję Pliki cookie w naszej Polityce prywatności.</p>
    <h2>Wykorzystanie Twoich danych osobowych</h2>
    <p>Firma może wykorzystywać Dane Osobowe do następujących celów:</p>
    <ul>
        <li><strong>Aby świadczyć i utrzymywać naszą Usługę</strong> , w tym monitorować korzystanie z naszej Usługi.
        </li>
        <li><strong>Aby zarządzać Twoim kontem:</strong> aby zarządzać Twoją rejestracją jako użytkownika Usługi. Podane
            przez Ciebie Dane Osobowe mogą zapewnić Ci dostęp do różnych funkcjonalności Serwisu, które są dostępne dla
            Ciebie jako zarejestrowanego użytkownika.
        </li>
        <li><strong>W celu wykonania umowy:</strong> opracowanie, przestrzeganie i podjęcie umowy zakupu produktów,
            przedmiotów lub usług, które kupiłeś lub jakiejkolwiek innej umowy z nami za pośrednictwem Usługi.
        </li>
        <li><strong>Aby skontaktować się z Tobą:</strong> aby skontaktować się z Tobą za pośrednictwem poczty
            elektronicznej, połączeń telefonicznych, SMS-ów lub innych równoważnych form komunikacji elektronicznej,
            takich jak powiadomienia push aplikacji mobilnej dotyczące aktualizacji lub wiadomości informacyjne związane
            z funkcjami, produktami lub zakontraktowanymi usługami, w tym aktualizacjami zabezpieczeń, gdy jest to
            konieczne lub uzasadnione dla ich realizacji.
        </li>
        <li><strong>Aby dostarczać Ci</strong> wiadomości, oferty specjalne i ogólne informacje o innych oferowanych
            przez nas towarach, usługach i wydarzeniach, które są podobne do tych, które już kupiłeś lub o które
            pytałeś, chyba że zdecydowałeś się nie otrzymywać takich informacji.
        </li>
        <li><strong>Aby zarządzać Twoimi prośbami:</strong> Aby uczestniczyć i zarządzać Twoimi prośbami kierowanymi do
            nas.
        </li>
        <li><strong>W przypadku transferów biznesowych:</strong> Możemy wykorzystywać Twoje dane do oceny lub
            przeprowadzenia fuzji, zbycia, restrukturyzacji, reorganizacji, rozwiązania lub innej sprzedaży lub
            przeniesienia części lub wszystkich naszych aktywów, czy to w ramach kontynuacji działalności, czy w ramach
            upadłości, likwidacji, lub podobne postępowanie, w którym wśród przenoszonych aktywów znajdują się posiadane
            przez nas Dane Osobowe dotyczące użytkowników naszych Usług.
        </li>
        <li><strong>Do innych celów</strong> : Możemy wykorzystywać Twoje informacje do innych celów, takich jak analiza
            danych, identyfikacja trendów użytkowania, określanie skuteczności naszych kampanii promocyjnych oraz ocena
            i ulepszanie naszej Usługi, produktów, usług, marketingu i Twoich doświadczeń.
        </li>

    </ul>
    <p>Możemy udostępniać Twoje dane osobowe w następujących sytuacjach:</p>
    <ul>
        <li><strong>Usługodawcom:</strong> Możemy udostępniać Twoje dane osobowe Usługodawcom w celu monitorowania i
            analizowania korzystania z naszej Usługi oraz kontaktowania się z Tobą.
        </li>
        <li><strong>W przypadku transferów biznesowych:</strong> Możemy udostępniać lub przekazywać Twoje dane osobowe w
            związku z negocjacjami fuzji, sprzedaży aktywów Spółki, finansowania lub przejęcia całości lub części naszej
            działalności innej firmie lub w trakcie negocjacji.
        </li>
        <li><strong>Podmiotom stowarzyszonym:</strong> Możemy udostępniać Twoje dane naszym podmiotom stowarzyszonym, w
            takim przypadku będziemy wymagać od tych podmiotów stowarzyszonych przestrzegania niniejszej Polityki
            prywatności. Spółki stowarzyszone obejmują naszą spółkę macierzystą i wszelkie inne spółki zależne,
            partnerów joint venture lub inne firmy, które kontrolujemy lub które znajdują się pod wspólną kontrolą z
            nami.
        </li>
        <li><strong>Partnerom biznesowym:</strong> Możemy udostępniać Twoje dane naszym partnerom biznesowym, aby
            oferować Ci określone produkty, usługi lub promocje.
        </li>
        <li><strong>Z innymi użytkownikami:</strong> kiedy udostępniasz dane osobowe lub w inny sposób wchodzisz w
            interakcje w obszarach publicznych z innymi użytkownikami, takie informacje mogą być przeglądane przez
            wszystkich użytkowników i mogą być publicznie rozpowszechniane na zewnątrz.
        </li>
        <li><strong>Za Twoją zgodą</strong> : Za Twoją zgodą możemy ujawnić Twoje dane osobowe w dowolnym innym celu.
        </li>

    </ul>
    <h2>Przechowywanie Twoich danych osobowych</h2>
    <p>Firma będzie przechowywać Twoje dane osobowe tylko tak długo, jak będzie to konieczne do celów określonych w
        niniejszej Polityce prywatności. Będziemy przechowywać i wykorzystywać Twoje Dane Osobowe w zakresie niezbędnym
        do wypełnienia naszych zobowiązań prawnych (na przykład, jeśli jesteśmy zobowiązani do zachowania Twoich danych
        w celu zachowania zgodności z obowiązującymi przepisami prawa), rozstrzygania sporów i egzekwowania naszych umów
        prawnych i zasad.</p>
    <p>Firma będzie również przechowywać Dane o użytkowaniu do celów analizy wewnętrznej. Dane dotyczące użytkowania są
        zasadniczo przechowywane przez krótszy okres czasu, z wyjątkiem sytuacji, gdy dane te są wykorzystywane do
        wzmocnienia bezpieczeństwa lub poprawy funkcjonalności naszej Usługi lub gdy jesteśmy prawnie zobowiązani do
        przechowywania tych danych przez dłuższy czas.</p>
    <h2>Przekazywanie Twoich danych osobowych</h2>
    <p>Twoje informacje, w tym Dane Osobowe, są przetwarzane w biurach operacyjnych Spółki oraz w innych miejscach, w
        których znajdują się strony zaangażowane w przetwarzanie. Oznacza to, że informacje te mogą być przesyłane — i
        przechowywane na — komputerach znajdujących się poza Twoim stanem, prowincją, krajem lub inną jurysdykcją
        rządową, w której przepisy dotyczące ochrony danych mogą różnić się od przepisów obowiązujących w Twojej
        jurysdykcji.</p>
    <p>Twoja zgoda na niniejszą Politykę prywatności, po której następuje przekazanie takich informacji, oznacza Twoją
        zgodę na ten transfer.</p>
    <p>Firma podejmie wszelkie uzasadnione kroki, aby zapewnić, że Twoje dane są traktowane bezpiecznie i zgodnie z
        niniejszą Polityką prywatności, a żadne przekazanie Twoich danych osobowych nie będzie miało miejsca do
        organizacji lub kraju, chyba że istnieją odpowiednie kontrole, w tym bezpieczeństwo Twoje dane i inne dane
        osobowe.</p>
    <h2>Usuń swoje dane osobowe</h2>
    <p>Masz prawo do usunięcia lub zażądania, abyśmy pomogli w usunięciu danych osobowych, które zebraliśmy na Twój
        temat.</p>
    <p>Nasza Usługa może dać Ci możliwość usunięcia pewnych informacji o Tobie z poziomu Usługi.</p>
    <p>Możesz aktualizować, zmieniać lub usuwać swoje dane w dowolnym momencie, logując się na swoje Konto, jeśli je
        posiadasz, i odwiedzając sekcję ustawień konta, która umożliwia zarządzanie Twoimi danymi osobowymi. Możesz
        również skontaktować się z nami, aby poprosić o dostęp, poprawienie lub usunięcie wszelkich danych osobowych,
        które nam przekazałeś.</p>
    <p>Należy jednak pamiętać, że możemy być zmuszeni do zachowania pewnych informacji, gdy mamy do tego obowiązek
        prawny lub podstawę prawną.</p>
    <h2>Ujawnienie Twoich danych osobowych</h2>
    <h3>Transakcje biznesowe</h3>
    <p>Jeśli Firma jest zaangażowana w fuzję, przejęcie lub sprzedaż aktywów, Twoje Dane Osobowe mogą zostać przekazane.
        Prześlemy powiadomienie, zanim Twoje dane osobowe zostaną przekazane i staną się przedmiotem innej Polityki
        prywatności.</p>
    <h3>Egzekwowanie prawa</h3>
    <p>W pewnych okolicznościach Firma może być zobowiązana do ujawnienia Twoich Danych Osobowych, jeśli jest do tego
        zobowiązana przez prawo lub w odpowiedzi na uzasadnione żądania władz publicznych (np. sądu lub agencji
        rządowej).</p>
    <h3>Inne wymogi prawne</h3>
    <p>Firma może ujawnić Twoje Dane Osobowe w dobrej wierze, że takie działanie jest niezbędne do:</p>
    <ul>
        <li>Wypełnij obowiązek prawny</li>
        <li>Ochrona i obrona praw lub własności Firmy</li>
        <li>Zapobiegać lub badać możliwe wykroczenia w związku z Usługą</li>
        <li>Ochrona bezpieczeństwa osobistego Użytkowników Serwisu lub społeczeństwa</li>
        <li>Chroń się przed odpowiedzialnością prawną</li>

    </ul>
    <h2>Bezpieczeństwo Twoich danych osobowych</h2>
    <p>Bezpieczeństwo Twoich Danych Osobowych jest dla nas ważne, ale pamiętaj, że żadna metoda przesyłania przez
        Internet, ani metoda elektronicznego przechowywania nie jest w 100% bezpieczna. Chociaż staramy się stosować
        komercyjnie akceptowalne środki w celu ochrony Twoich danych osobowych, nie możemy zagwarantować ich całkowitego
        bezpieczeństwa.</p>
    <h1>Prywatność dzieci</h1>
    <p>Nasza Usługa nie jest skierowana do osób poniżej 13 roku życia. Nie zbieramy świadomie danych osobowych osób
        poniżej 13 roku życia. Jeśli jesteś rodzicem lub opiekunem i wiesz, że Twoje dziecko przekazało nam Dane
        Osobowe, prosimy Skontaktuj się z nami. Jeśli dowiemy się, że zebraliśmy Dane osobowe od osób poniżej 13 roku
        życia bez weryfikacji zgody rodziców, podejmiemy kroki w celu usunięcia tych informacji z naszych serwerów.</p>
    <p>Jeśli musimy polegać na zgodzie jako podstawie prawnej do przetwarzania Twoich danych, a Twój kraj wymaga zgody
        rodzica, możemy wymagać zgody Twojego rodzica, zanim zbierzemy i wykorzystamy te informacje.</p>
    <h1>Linki do innych stron internetowych</h1>
    <p>Nasz Serwis może zawierać linki do innych stron internetowych, które nie są przez nas obsługiwane. Jeśli
        klikniesz łącze strony trzeciej, zostaniesz przekierowany na stronę tej osoby trzeciej. Zdecydowanie zalecamy
        zapoznanie się z Polityką prywatności każdej odwiedzanej witryny.</p>
    <p>Nie mamy kontroli i nie ponosimy żadnej odpowiedzialności za treść, politykę prywatności lub praktyki
        jakichkolwiek witryn lub usług stron trzecich.</p>
    <h1>Zmiany w niniejszej Polityce Prywatności</h1>
    <p>Od czasu do czasu możemy aktualizować naszą Politykę prywatności. Powiadomimy Cię o wszelkich zmianach,
        publikując nową Politykę prywatności na tej stronie.</p>
    <p>Powiadomimy Cię o tym za pośrednictwem poczty elektronicznej i/lub widocznego powiadomienia w naszej usłudze,
        zanim zmiana wejdzie w życie, i zaktualizujemy datę „Ostatnia aktualizacja” na górze niniejszej Polityki
        prywatności.</p>
    <p>Zaleca się okresowe przeglądanie niniejszej Polityki prywatności pod kątem wszelkich zmian. Zmiany w niniejszej
        Polityce Prywatności wchodzą w życie z chwilą ich opublikowania na tej stronie.</p>
</article>
