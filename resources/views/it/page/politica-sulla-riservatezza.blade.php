<article>
    <h1>Politica sulla riservatezza</h1>
    <p>Ultimo aggiornamento: 26 marzo 2023</p>
    <h1>Interpretazione e definizioni</h1>
    <h2>Interpretazione</h2>
    <p>Le parole la cui lettera iniziale è maiuscola hanno significati definiti alle seguenti condizioni. Le seguenti
        definizioni hanno lo stesso significato indipendentemente dal fatto che appaiano al singolare o al plurale.</p>
    <h2>Definizioni</h2>
    <p>Ai fini della presente Informativa sulla privacy:</p>
    <ul>
        <li><strong>Account</strong> indica un account univoco creato per te per accedere al nostro Servizio o a parti
            del nostro Servizio.
        </li>
        <li><strong>Affiliata</strong> indica un&#39;entità che controlla, è controllata da o è soggetta a controllo
            comune con una parte, dove &quot;controllo&quot; indica la proprietà del 50% o più delle azioni,
            partecipazioni azionarie o altri titoli aventi diritto di voto per l&#39;elezione degli amministratori o di
            altra autorità di gestione .
        </li>
        <li><strong>La Società</strong> (indicata come &quot;la Società&quot;, &quot;Noi&quot;, &quot;Noi&quot; o &quot;Nostro&quot;
            nel presente Accordo) si riferisce a KosmCODE Maksym Kawelski, Małopolska 49/14, 70-514 Szczecin.
        </li>
        <li><strong>I cookie</strong> sono piccoli file che vengono inseriti sul tuo computer, dispositivo mobile o
            qualsiasi altro dispositivo da un sito Web, contenente i dettagli della tua cronologia di navigazione su
            quel sito Web tra i suoi numerosi usi.
        </li>
        <li><strong>Paese</strong> si riferisce a: Polonia</li>
        <li><strong>Dispositivo</strong> indica qualsiasi dispositivo che può accedere al Servizio come un computer, un
            cellulare o un tablet digitale.
        </li>
        <li><strong>I dati personali</strong> sono tutte le informazioni relative a un individuo identificato o
            identificabile.
        </li>
        <li><strong>Il servizio</strong> si riferisce al sito web.</li>
        <li><strong>Fornitore di servizi</strong> indica qualsiasi persona fisica o giuridica che elabora i dati per
            conto della Società. Si riferisce a società o persone terze impiegate dalla Società per facilitare il
            Servizio, per fornire il Servizio per conto della Società, per eseguire servizi relativi al Servizio o per
            assistere la Società nell&#39;analisi di come viene utilizzato il Servizio.
        </li>
        <li><strong>I Dati di utilizzo</strong> si riferiscono ai dati raccolti automaticamente, generati dall&#39;uso
            del Servizio o dall&#39;infrastruttura del Servizio stesso (ad esempio, la durata di una visita alla
            pagina).
        </li>
        <li><strong>Il sito Web</strong> fa riferimento a Smart Password, accessibile da <a
                href='https://www.smart-password.net/' target='_blank' class='url'>https://www.smart-password.net/</a>
        </li>
        <li><strong>Si</strong> intende la persona che accede o utilizza il Servizio, o la società o altra entità legale
            per conto della quale tale persona accede o utilizza il Servizio, a seconda dei casi.
        </li>

    </ul>
    <h1>Raccolta e utilizzo dei dati personali</h1>
    <h2>Tipi di dati raccolti</h2>
    <h3>Dati personali</h3>
    <p>Durante l&#39;utilizzo del nostro servizio, potremmo chiederti di fornirci alcune informazioni di identificazione
        personale che possono essere utilizzate per contattarti o identificarti. Le informazioni di identificazione
        personale possono includere, ma non sono limitate a:</p>
    <ul>
        <li>Indirizzo e-mail</li>
        <li>Dati di utilizzo</li>

    </ul>
    <h3>Dati di utilizzo</h3>
    <p>I Dati di utilizzo vengono raccolti automaticamente durante l&#39;utilizzo del Servizio.</p>
    <p>I Dati di utilizzo possono includere informazioni quali l&#39;indirizzo del protocollo Internet del tuo
        dispositivo (ad esempio l&#39;indirizzo IP), il tipo di browser, la versione del browser, le pagine del nostro
        servizio che visiti, l&#39;ora e la data della tua visita, il tempo trascorso su tali pagine, il dispositivo
        unico identificatori e altri dati diagnostici.</p>
    <p>Quando accedi al Servizio da o tramite un dispositivo mobile, potremmo raccogliere determinate informazioni
        automaticamente, inclusi, a titolo esemplificativo ma non esaustivo, il tipo di dispositivo mobile che utilizzi,
        l&#39;ID univoco del tuo dispositivo mobile, l&#39;indirizzo IP del tuo dispositivo mobile, sistema operativo,
        il tipo di browser Internet mobile utilizzato, identificatori univoci del dispositivo e altri dati
        diagnostici.</p>
    <p>Potremmo anche raccogliere informazioni che il tuo browser invia ogni volta che visiti il nostro Servizio o
        quando accedi al Servizio da o attraverso un dispositivo mobile.</p>
    <h3>Tecnologie di tracciamento e cookie</h3>
    <p>Utilizziamo cookie e tecnologie di tracciamento simili per tracciare l&#39;attività sul nostro servizio e
        archiviare determinate informazioni. Le tecnologie di tracciamento utilizzate sono beacon, tag e script per
        raccogliere e tenere traccia delle informazioni e per migliorare e analizzare il nostro servizio. Le tecnologie
        che utilizziamo possono includere:</p>
    <ul>
        <li><strong>Cookie o Cookie del browser.</strong> Un cookie è un piccolo file posizionato sul tuo dispositivo.
            Puoi istruire il tuo browser a rifiutare tutti i cookie o a indicare quando viene inviato un cookie.
            Tuttavia, se non accetti i cookie, potresti non essere in grado di utilizzare alcune parti del nostro
            servizio. A meno che tu non abbia regolato le impostazioni del tuo browser in modo tale da rifiutare i
            cookie, il nostro Servizio potrebbe utilizzare i cookie.
        </li>
        <li><strong>Web beacon.</strong> Alcune sezioni del nostro Servizio e le nostre e-mail possono contenere piccoli
            file elettronici noti come web beacon (noti anche come clear gif, pixel tag e single-pixel gif) che
            consentono alla Società, ad esempio, di contare gli utenti che hanno visitato tali pagine o ha aperto un&#39;e-mail
            e per altre statistiche relative al sito Web (ad esempio, registrando la popolarità di una determinata
            sezione e verificando l&#39;integrità del sistema e del server).
        </li>

    </ul>
    <p>I cookie possono essere &quot;persistenti&quot; o &quot;di sessione&quot;. I cookie persistenti rimangono sul tuo
        personal computer o dispositivo mobile quando vai offline, mentre i cookie di sessione vengono eliminati non
        appena chiudi il browser web. Ulteriori informazioni sui cookie nell&#39;articolo del <a
            href='https://www.freeprivacypolicy.com/blog/sample-privacy-policy-template/#Use_Of_Cookies_And_Tracking'>sito
            Web sulla politica sulla privacy gratuita</a> .</p>
    <p>Utilizziamo sia i cookie di sessione che quelli persistenti per gli scopi indicati di seguito:</p>
    <ul>
        <li><p><strong>Cookie necessari / essenziali</strong></p>
            <p>Tipo: Cookie di sessione</p>
            <p>Gestito da: Noi</p>
            <p>Scopo: questi cookie sono essenziali per fornirti i servizi disponibili attraverso il sito Web e per
                consentirti di utilizzare alcune delle sue funzionalità. Aiutano ad autenticare gli utenti e prevenire l&#39;uso
                fraudolento degli account utente. Senza questi cookie, i servizi che hai richiesto non possono essere
                forniti e utilizziamo questi cookie solo per fornirti tali servizi.</p>
        </li>
        <li><p><strong>Cookies Policy / Avviso Accettazione Cookies</strong></p>
            <p>Tipo: cookie persistenti</p>
            <p>Gestito da: Noi</p>
            <p>Scopo: questi cookie identificano se gli utenti hanno accettato l&#39;uso dei cookie sul sito web.</p>
        </li>
        <li><p><strong>Cookie di funzionalità</strong></p>
            <p>Tipo: cookie persistenti</p>
            <p>Gestito da: Noi</p>
            <p>Scopo: questi cookie ci consentono di ricordare le scelte effettuate dall&#39;utente durante l&#39;utilizzo
                del sito Web, ad esempio ricordare i dettagli di accesso o la lingua preferita. Lo scopo di questi
                cookie è di fornirti un&#39;esperienza più personale e di evitare che tu debba reinserire le tue
                preferenze ogni volta che usi il sito web.</p>
        </li>

    </ul>
    <p>Per ulteriori informazioni sui cookie che utilizziamo e sulle tue scelte in merito ai cookie, visita la nostra
        Politica sui cookie o la sezione Cookie della nostra Politica sulla privacy.</p>
    <h2>Uso dei tuoi dati personali</h2>
    <p>La Società può utilizzare i Dati Personali per le seguenti finalità:</p>
    <ul>
        <li><strong>Per fornire e mantenere il nostro Servizio</strong> , incluso per monitorare l&#39;utilizzo del
            nostro Servizio.
        </li>
        <li><strong>Per gestire il tuo account:</strong> per gestire la tua registrazione come utente del servizio. I
            Dati Personali che fornisci possono darti accesso a diverse funzionalità del Servizio che sono a tua
            disposizione come utente registrato.
        </li>
        <li><strong>Per l&#39;esecuzione di un contratto:</strong> lo sviluppo, la conformità e l&#39;esecuzione del
            contratto di acquisto per i prodotti, articoli o servizi che hai acquistato o di qualsiasi altro contratto
            con Noi attraverso il Servizio.
        </li>
        <li><strong>Per contattarti:</strong> per contattarti tramite e-mail, telefonate, SMS o altre forme equivalenti
            di comunicazione elettronica, come notifiche push di un&#39;applicazione mobile relative ad aggiornamenti o
            comunicazioni informative relative alle funzionalità, ai prodotti o ai servizi contrattati, inclusi gli
            aggiornamenti di sicurezza, quando necessario o ragionevole per la loro attuazione.
        </li>
        <li><strong>Per fornirti</strong> notizie, offerte speciali e informazioni generali su altri beni, servizi ed
            eventi che offriamo simili a quelli che hai già acquistato o richiesto, a meno che tu non abbia scelto di
            non ricevere tali informazioni.
        </li>
        <li><strong>Per gestire le tue richieste:</strong> per partecipare e gestire le tue richieste a noi.</li>
        <li><strong>Per i trasferimenti aziendali:</strong> possiamo utilizzare le informazioni dell&#39;utente per
            valutare o condurre una fusione, cessione, ristrutturazione, riorganizzazione, scioglimento o altra vendita
            o trasferimento di alcuni o tutti i nostri beni, sia come impresa in attività che come parte di un
            fallimento, liquidazione, o procedimento simile, in cui i Dati personali da noi detenuti sugli utenti del
            nostro Servizio sono tra i beni trasferiti.
        </li>
        <li><strong>Per altri scopi</strong> : potremmo utilizzare le tue informazioni per altri scopi, come l&#39;analisi
            dei dati, l&#39;identificazione delle tendenze di utilizzo, la determinazione dell&#39;efficacia delle
            nostre campagne promozionali e per valutare e migliorare il nostro servizio, i prodotti, i servizi, il
            marketing e la tua esperienza.
        </li>

    </ul>
    <p>Potremmo condividere le tue informazioni personali nelle seguenti situazioni:</p>
    <ul>
        <li><strong>Con i fornitori di servizi:</strong> potremmo condividere le tue informazioni personali con i
            fornitori di servizi per monitorare e analizzare l&#39;utilizzo del nostro servizio, per contattarti.
        </li>
        <li><strong>Per i trasferimenti aziendali:</strong> possiamo condividere o trasferire le tue informazioni
            personali in relazione a, o durante le negoziazioni di, qualsiasi fusione, vendita di beni della Società,
            finanziamento o acquisizione di tutta o parte della nostra attività a un&#39;altra società.
        </li>
        <li><strong>Con gli affiliati:</strong> potremmo condividere le tue informazioni con i nostri affiliati, nel
            qual caso richiederemo a tali affiliati di onorare la presente Informativa sulla privacy. Le affiliate
            includono la nostra società madre e qualsiasi altra sussidiaria, partner di joint venture o altre società
            che controlliamo o che sono sotto controllo comune con noi.
        </li>
        <li><strong>Con i partner commerciali:</strong> potremmo condividere le tue informazioni con i nostri partner
            commerciali per offrirti determinati prodotti, servizi o promozioni.
        </li>
        <li><strong>Con altri utenti:</strong> quando condividi informazioni personali o altrimenti interagisci nelle
            aree pubbliche con altri utenti, tali informazioni possono essere visualizzate da tutti gli utenti e possono
            essere distribuite pubblicamente all&#39;esterno.
        </li>
        <li><strong>Con il tuo consenso</strong> : possiamo divulgare le tue informazioni personali per qualsiasi altro
            scopo con il tuo consenso.
        </li>

    </ul>
    <h2>Conservazione dei tuoi dati personali</h2>
    <p>La Società conserverà i tuoi dati personali solo per il tempo necessario agli scopi indicati nella presente
        Informativa sulla privacy. Conserveremo e utilizzeremo i tuoi dati personali nella misura necessaria per
        adempiere ai nostri obblighi legali (ad esempio, se ci viene richiesto di conservare i tuoi dati per rispettare
        le leggi applicabili), risolvere controversie e far rispettare i nostri accordi e politiche legali.</p>
    <p>La Società conserverà inoltre i Dati di utilizzo a fini di analisi interna. I dati di utilizzo vengono
        generalmente conservati per un periodo di tempo più breve, tranne quando questi dati vengono utilizzati per
        rafforzare la sicurezza o per migliorare la funzionalità del nostro servizio, o siamo legalmente obbligati a
        conservare questi dati per periodi di tempo più lunghi.</p>
    <h2>Trasferimento dei tuoi dati personali</h2>
    <p>Le tue informazioni, inclusi i Dati Personali, sono trattate presso le sedi operative della Società ed in ogni
        altro luogo in cui si trovino le parti coinvolte nel trattamento. Significa che queste informazioni possono
        essere trasferite a - e mantenute su - computer situati al di fuori del tuo stato, provincia, paese o altra
        giurisdizione governativa in cui le leggi sulla protezione dei dati possono differire da quelle della tua
        giurisdizione.</p>
    <p>Il tuo consenso alla presente Informativa sulla privacy seguito dall&#39;invio di tali informazioni rappresenta
        il tuo consenso a tale trasferimento.</p>
    <p>La Società adotterà tutte le misure ragionevolmente necessarie per garantire che i tuoi dati siano trattati in
        modo sicuro e in conformità con la presente Informativa sulla privacy e nessun trasferimento dei tuoi dati
        personali avrà luogo a un&#39;organizzazione o a un paese a meno che non siano in atto controlli adeguati,
        inclusa la sicurezza di I tuoi dati e altre informazioni personali.</p>
    <h2>Elimina i tuoi dati personali</h2>
    <p>Hai il diritto di eliminare o richiedere la nostra assistenza nell&#39;eliminazione dei dati personali che
        abbiamo raccolto su di te.</p>
    <p>Il nostro Servizio può darti la possibilità di eliminare determinate informazioni su di te dall&#39;interno del
        Servizio.</p>
    <p>Puoi aggiornare, modificare o eliminare le tue informazioni in qualsiasi momento accedendo al tuo account, se ne
        hai uno, e visitando la sezione delle impostazioni dell&#39;account che ti consente di gestire le tue
        informazioni personali. Puoi anche contattarci per richiedere l&#39;accesso, la correzione o l&#39;eliminazione
        di qualsiasi informazione personale che ci hai fornito.</p>
    <p>Si prega di notare, tuttavia, che potremmo aver bisogno di conservare determinate informazioni quando abbiamo un
        obbligo legale o una base legale per farlo.</p>
    <h2>Divulgazione dei tuoi dati personali</h2>
    <h3>Transazione d&#39;affari</h3>
    <p>Se la Società è coinvolta in una fusione, acquisizione o vendita di beni, i tuoi dati personali potrebbero essere
        trasferiti. Forniremo un avviso prima che i tuoi dati personali vengano trasferiti e diventino soggetti a una
        diversa Informativa sulla privacy.</p>
    <h3>Applicazione della legge</h3>
    <p>In determinate circostanze, la Società potrebbe essere tenuta a divulgare i tuoi dati personali se richiesto
        dalla legge o in risposta a richieste valide da parte delle autorità pubbliche (ad esempio un tribunale o un&#39;agenzia
        governativa).</p>
    <h3>Altri requisiti legali</h3>
    <p>La Società può divulgare i tuoi dati personali in buona fede ritenendo che tale azione sia necessaria per:</p>
    <ul>
        <li>Rispettare un obbligo legale</li>
        <li>Proteggere e difendere i diritti o la proprietà della Società</li>
        <li>Prevenire o indagare su possibili illeciti in relazione al Servizio</li>
        <li>Proteggere la sicurezza personale degli Utenti del Servizio o del pubblico</li>
        <li>Protezione contro la responsabilità legale</li>

    </ul>
    <h2>Sicurezza dei tuoi dati personali</h2>
    <p>La sicurezza dei tuoi dati personali è importante per noi, ma ricorda che nessun metodo di trasmissione su
        Internet o metodo di archiviazione elettronica è sicuro al 100%. Sebbene ci sforziamo di utilizzare mezzi
        commercialmente accettabili per proteggere i tuoi dati personali, non possiamo garantirne l&#39;assoluta
        sicurezza.</p>
    <h1>Privacy dei bambini</h1>
    <p>Il nostro servizio non si rivolge a persone di età inferiore ai 13 anni. Non raccogliamo consapevolmente
        informazioni di identificazione personale da persone di età inferiore ai 13 anni. Se sei un genitore o tutore e
        sei consapevole che tuo figlio ci ha fornito dati personali, ti preghiamo di Contattaci. Se veniamo a conoscenza
        del fatto che abbiamo raccolto dati personali da persone di età inferiore ai 13 anni senza la verifica del
        consenso dei genitori, adottiamo misure per rimuovere tali informazioni dai nostri server.</p>
    <p>Se dobbiamo fare affidamento sul consenso come base legale per l&#39;elaborazione delle tue informazioni e il tuo
        paese richiede il consenso di un genitore, potremmo richiedere il consenso dei tuoi genitori prima di
        raccogliere e utilizzare tali informazioni.</p>
    <h1>Link ad altri siti web</h1>
    <p>Il nostro servizio può contenere collegamenti ad altri siti Web che non sono gestiti da noi. Se fai clic su un
        collegamento di terze parti, verrai indirizzato al sito di tale terza parte. Ti consigliamo vivamente di
        rivedere l&#39;Informativa sulla privacy di ogni sito che visiti.</p>
    <p>Non abbiamo alcun controllo e non ci assumiamo alcuna responsabilità per il contenuto, le politiche sulla privacy
        o le pratiche di qualsiasi sito o servizio di terze parti.</p>
    <h1>Modifiche alla presente Informativa sulla privacy</h1>
    <p>Potremmo aggiornare la nostra Informativa sulla privacy di volta in volta. Ti informeremo di eventuali modifiche
        pubblicando la nuova Informativa sulla privacy in questa pagina.</p>
    <p>Ti informeremo via e-mail e/o un avviso ben visibile sul Nostro Servizio, prima che la modifica diventi effettiva
        e aggiorneremo la data dell&#39;&quot;Ultimo aggiornamento&quot; nella parte superiore della presente
        Informativa sulla privacy.</p>
    <p>Si consiglia di rivedere periodicamente la presente Informativa sulla privacy per eventuali modifiche. Le
        modifiche alla presente Informativa sulla privacy entrano in vigore quando vengono pubblicate su questa
        pagina.</p>
</article>
