<?php

use App\Http\Controllers\CookieConsentsController;
use App\Http\Controllers\MainController;
use App\Http\Controllers\PasswordsController;
use App\Http\Controllers\SimplePageController;
use App\Http\Middleware\HandleInertiaRequests;
use App\Http\Middleware\LocaleMiddleware;
use App\Http\Middleware\LocaleRedirectMiddleware;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('', [MainController::class, 'getMainPage'])
    ->middleware([LocaleMiddleware::class, HandleInertiaRequests::class]);

Route::name('cookie')
    ->prefix('/cookie')
    ->group(function () {
        Route::name('.consents')
            ->prefix('/consents')
            ->group(function () {
                Route::put('accepted', [CookieConsentsController::class, 'accepted'])
                    ->name('.accepted');
                Route::get('dontAccepted', [CookieConsentsController::class, 'dontAccepted'])
                    ->name('.dontAccepted');
            });
    });

Route::prefix('/{lang}')
    ->whereIn('lang', Config::get('app.availableLocales'))
    ->middleware([LocaleMiddleware::class, HandleInertiaRequests::class])
    ->group(function () {
        Route::get('/', [MainController::class, 'getMainPage'])
            ->name('main');
        Route::get('/{slug}', [SimplePageController::class, 'getByLangAndSlug'])
            ->name('simplePage');
    });

Route::name('passwords')
    ->prefix('/passwords')
    ->middleware('throttle:20,1')
    ->group(function () {
        Route::post('/dictionaries', [PasswordsController::class, 'generateFromDictionaries'])
            ->name('.dictionaries')
            ->middleware(LocaleMiddleware::class);
        Route::get('/dictionaries/config', [PasswordsController::class, 'getDictionariesConfig'])
            ->name('.dictionaries.config')
            ->middleware(LocaleMiddleware::class);
        Route::post('/score', [PasswordsController::class, 'getPasswordScore'])
            ->name('.score')
            ->middleware(LocaleMiddleware::class);
        Route::get('/random/config', [PasswordsController::class, 'getRandomPasswordConfig'])
            ->name('.random.config')
            ->middleware(LocaleMiddleware::class);
        Route::post('/random', [PasswordsController::class, 'generateRandomPasswords'])
            ->name('.random')
            ->middleware(LocaleMiddleware::class);
    });

Route::get('{any}')
    ->middleware([LocaleRedirectMiddleware::class]);
