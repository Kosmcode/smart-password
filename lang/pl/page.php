<?php

return [
    'availableCharacters' => 'Możliwe znaki',
    'breakTimeInSeconds' => 'Czas złamania hasła',
    'camelCase' => 'Małe i duże litery',
    'checkPasswordScore' => 'Sprawdź siłę hasła',
    'copied' => 'Skopiowano',
    'countOfPasswords' => 'Ilość haseł do wygenerowania',
    'dictionaries' => 'Słowniki',
    'english' => 'Angielski',
    'french' => 'Francuski',
    'generate' => 'Generuj',
    'generatedPassword' => 'Hasło generowane',
    'generatedPasswordAbout' => 'Generator haseł losowych pozwoli ći wygenerować hasło zawierające dane znaki.',
    'generatedPasswords' => 'Wygenerowane hasła',
    'german' => 'Niemiecki',
    'head' => [
        'meta' => [
            'description' => 'Smart Password pozwoli Ci wygenerować silne, trudne do złamania hasło oraz łatwe do zapamiętania.',
            'keywords' => 'Smart, Password, Hasło, generator, haseł',
        ],
        'title' => 'Smart-Password.net - wygeneruj swoje hasło!',
    ],
    'italian' => 'Włoski',
    'language' => 'Język',
    'languages' => 'Języki',
    'legendStrengthPasswords' => 'Legenda:',
    'length' => 'Długość',
    'max' => 'Max',
    'min' => 'Min',
    'passwordCheckScore' => 'Hasło do sprawdzenia',
    'polish' => 'Polski',
    'result' => 'Rezultat',
    'scorePassword' => 'Ocena hasła',
    'scorePasswordAbout' => 'Ocena hasła pozwoli ci sprawdzić siłe danego hasła oraz ocenić przewidywany czas potrzebny do jego złamania.',
    'smartPassword' => 'SMART hasło',
    'smartPasswordAbout' => 'SMART hasło pozwoli ci wygenerować silne hasło, trudne do złamania, a zarazem łatwe do zapamiętania.',
    'spanish' => 'Hiszpański',
    'wait' => 'Czekaj',
    'privacyPolicy' => [
        'label' => 'Polityka prywatności',
        'slug' => 'polityka-prywatnosci',
    ],
];
