<?php

return [
    'consents' => [
        'modal' => [
            'acceptButtonLabel' => 'Akceptuje',
            'notAcceptButtonLabel' => 'Nie akceptuje',
            'text' => 'Strona wykorzystuje ciasteczka do zapewnienia jak najleszych doświadczeń.',
            'urlText' => 'Więcej o ciasteczkach',
        ],
    ],
];
