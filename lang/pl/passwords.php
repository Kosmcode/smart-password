<?php

return [
    'numbers' => 'Cyfry',
    'rate' => [
        'score' => [
            '0' => 'Hasło niezwykle łatwe do odgadnięcia (w granicach 10^3 prób) - np. słowa słownikowe, takie jak `hasło` lub `matka`',
            '1' => 'Nadal łatwe do odgadnięcia (domysły < 10^6) - zawiera np. dodatkowy znak',
            '2' => 'Do pewnego stopnia możliwe do odgadnięcia (zgadywanie < 10^8) - zapewnia względną ochronę przed atakami online',
            '3' => 'Względnie bezpieczne, trudne do odgadnięcia (domysły < 10^10) - zapewnia umiarkowaną ochronę',
            '4' => 'Bardzo bezpieczne, nie możliwe do odgadnięcia (domysły >= 10^10) - zapewnia silną ochronę',
        ],
        'timePer' => [
            'offline10000000000TriesPerSecond' => 'Offline 10000000000 prób na sekundę',
            'offline10000TriesPerSecond' => 'Offline 10000 prób na sekundę',
            'online100TriesPerHour' => '100 prób online na godzinę',
            'online10TriesPerSecond' => '10 prób online na sekundę',
        ],
    ],
    'reset' => 'Hasło zostało zresetowane!',
    'sent' => 'Przypomnienie hasła zostało wysłane!',
    'smallLetters' => 'Małe litery',
    'special' => 'Znaki specjalne',
    'throttled' => 'Proszę zaczekać zanim spróbujesz ponownie.',
    'token' => 'Token resetowania hasła jest nieprawidłowy.',
    'upperLetters' => 'Duże litery',
    'user' => 'Nie znaleziono użytkownika z takim adresem e-mail.',
];
