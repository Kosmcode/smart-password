<?php

return [
    'consents' => [
        'modal' => [
            'acceptButtonLabel' => 'Acceptez',
            'notAcceptButtonLabel' => 'Ne pas accepter',
            'text' => 'Ce site utilise des cookies pour vous garantir la meilleure expérience sur notre site.',
            'urlText' => 'En savoir plus sur les cookies',
        ],
    ],
];
