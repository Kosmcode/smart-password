<?php

return [
    'numbers' => 'Nombres',
    'rate' => [
        'score' => [
            '0' => 'Signifie que le mot de passe est extrêmement devinable (dans les 10 ^ 3 suppositions), des mots du dictionnaire comme `password` ou `mother`',
            '1' => 'Toujours très devinable (suppose < 10 ^ 6), un caractère supplémentaire sur un mot du dictionnaire',
            '2' => 'Un peu devinable (suppose < 10 ^ 8), offre une certaine protection contre les attaques en ligne non étranglées',
            '3' => 'En toute sécurité impossible à deviner (suppose < 10 ^ 10), offre une protection modérée contre le scénario de hachage lent hors ligne',
            '4' => 'Très impossible à deviner (suppose> = 10 ^ 10) et offre une protection solide contre le scénario de hachage lent hors ligne',
        ],
        'timePer' => [
            'offline10000000000TriesPerSecond' => 'Hors ligne 10000000000 tentatives par seconde :',
            'offline10000TriesPerSecond' => 'Hors ligne 10 000 essais par seconde :',
            'online100TriesPerHour' => 'En ligne 100 essais par heure :',
            'online10TriesPerSecond' => 'En ligne 10 essais par seconde :',
        ],
    ],
    'reset' => 'Votre mot de passe a été réinitialisé !',
    'sent' => 'Nous vous avons envoyé par email le lien de réinitialisation du mot de passe !',
    'smallLetters' => 'Lettres minuscules',
    'special' => 'Caractères spéciaux',
    'throttled' => 'Veuillez patienter avant de réessayer.',
    'token' => "Ce jeton de réinitialisation du mot de passe n'est pas valide.",
    'upperLetters' => 'Majuscules',
    'user' => "Aucun utilisateur n'a été trouvé avec cette adresse email.",
];
