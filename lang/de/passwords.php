<?php

return [
    'numbers' => 'ZAHLEN',
    'rate' => [
        'score' => [
            '0' => 'Means the password is extremely guessable (within 10^3 guesses), dictionary words like `password` or `mother`',
            '1' => 'Immer noch sehr erratbar (Schätzungen < 10^6), ein zusätzliches Zeichen bei einem Wörterbuchwort',
            '2' => 'Etwas erraten (Schätzungen < 10^8), bietet einen gewissen Schutz vor ungedrosselten Online-Angriffen',
            '3' => 'Sicher unschätzbar (Schätzungen < 10^10), bietet moderaten Schutz vor Offline-Slow-Hash-Szenarien',
            '4' => 'Sehr unschätzbar (schätzt >= 10^10) und bietet starken Schutz vor Offline-Slow-Hash-Szenarien',
        ],
        'timePer' => [
            'offline10000000000TriesPerSecond' => 'Offline 10000000000 Versuche pro Sekunde:',
            'offline10000TriesPerSecond' => 'Offline 10000 Versuche pro Sekunde:',
            'online100TriesPerHour' => 'Online 100 Versuche pro Stunde:',
            'online10TriesPerSecond' => 'Online 10 Versuche pro Sekunde:',
        ],
    ],
    'reset' => 'Das Passwort wurde zurückgesetzt!',
    'sent' => 'Passworterinnerung wurde gesendet!',
    'smallLetters' => 'Kleinbuchstaben',
    'special' => 'Spezielle Charaktere',
    'throttled' => 'Bitte warten Sie, bevor Sie es erneut versuchen.',
    'token' => 'Der Passwort-Wiederherstellungs-Schlüssel ist ungültig oder abgelaufen.',
    'upperLetters' => 'Großbuchstaben',
    'user' => 'Es konnte leider kein Nutzer mit dieser E-Mail-Adresse gefunden werden.',
];
