<?php

return [
    'consents' => [
        'modal' => [
            'acceptButtonLabel' => 'Akzeptieren',
            'notAcceptButtonLabel' => 'Akzeptiere nicht',
            'text' => 'Diese Website verwendet Cookies, um sicherzustellen, dass Sie das beste Erlebnis auf unserer Website erhalten.',
            'urlText' => 'Lesen Sie mehr über Cookies',
        ],
    ],
];
