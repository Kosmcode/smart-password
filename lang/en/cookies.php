<?php

return [
    'consents' => [
        'modal' => [
            'acceptButtonLabel' => 'Accept',
            'notAcceptButtonLabel' => 'Dont accept',
            'text' => 'This website uses cookies to ensure you get the best experience on our website.',
            'urlText' => 'Read more about cookies',
        ],
    ],
];
