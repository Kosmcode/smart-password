<?php

return [
    'numbers' => 'Numbers',
    'rate' => [
        'score' => [
            '0' => 'Means the password is extremely guessable (within 10^3 guesses), dictionary words like `password` or `mother`',
            '1' => 'Still very guessable (guesses < 10^6), an extra character on a dictionary word',
            '2' => 'Somewhat guessable (guesses < 10^8), provides some protection from unthrottled online attacks',
            '3' => 'Safely unguessable (guesses < 10^10), offers moderate protection from offline slow-hash scenario',
            '4' => 'Very unguessable (guesses >= 10^10) and provides strong protection from offline slow-hash scenario',
        ],
        'timePer' => [
            'offline10000000000TriesPerSecond' => 'Offline 10000000000 tries per second:',
            'offline10000TriesPerSecond' => 'Offline 10000 tries per second:',
            'online100TriesPerHour' => 'Online 100 tries per hour:',
            'online10TriesPerSecond' => 'Online 10 tries per second:',
        ],
    ],
    'reset' => 'Your password has been reset!',
    'sent' => 'We have emailed your password reset link!',
    'smallLetters' => 'Lower letters',
    'special' => 'Special characters',
    'throttled' => 'Please wait before retrying.',
    'token' => 'This password reset token is invalid.',
    'upperLetters' => 'Upper letters',
    'user' => "We can't find a user with that email address.",
];
