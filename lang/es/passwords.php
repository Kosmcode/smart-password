<?php

return [
    'numbers' => 'Números',
    'rate' => [
        'score' => [
            '0' => 'Significa que la contraseña es extremadamente fácil de adivinar (dentro de 10 ^ 3 intentos), palabras del diccionario como "contraseña" o "madre".',
            '1' => 'Todavía muy fácil de adivinar (conjeturas < 10^6), un carácter adicional en una palabra del diccionario',
            '2' => 'Algo adivinable (suposiciones < 10^8), brinda cierta protección contra ataques en línea sin control',
            '3' => 'Imposible de adivinar de forma segura (conjeturas < 10^10), ofrece una protección moderada frente al escenario de hash lento fuera de línea',
            '4' => 'Muy indescifrable (conjeturas >= 10^10) y proporciona una fuerte protección contra el escenario de hash lento fuera de línea',
        ],
        'timePer' => [
            'offline10000000000TriesPerSecond' => 'Desconectado 10000000000 intentos por segundo:',
            'offline10000TriesPerSecond' => 'Desconectado 10000 intentos por segundo:',
            'online100TriesPerHour' => 'En línea 100 intentos por hora:',
            'online10TriesPerSecond' => 'En línea 10 intentos por segundo:',
        ],
    ],
    'reset' => 'Su contraseña ha sido restablecida.',
    'sent' => 'Le hemos enviado por correo electrónico el enlace para restablecer su contraseña.',
    'smallLetters' => 'Letras minusculas',
    'special' => 'Caracteres especiales',
    'throttled' => 'Por favor espere antes de intentar de nuevo.',
    'token' => 'El token de restablecimiento de contraseña es inválido.',
    'upperLetters' => 'Letras mayúsculas',
    'user' => 'No encontramos ningún usuario con ese correo electrónico.',
];
