<?php

return [
    'consents' => [
        'modal' => [
            'acceptButtonLabel' => 'Aceptar',
            'notAcceptButtonLabel' => 'No aceptar',
            'text' => 'Este sitio web utiliza cookies para garantizar que obtenga la mejor experiencia en nuestro sitio web.',
            'urlText' => 'Leer más sobre las cookies',
        ],
    ],
];
