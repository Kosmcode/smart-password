<?php

return [
    'availableCharacters' => 'Caracteres disponibles',
    'breakTimeInSeconds' => 'Tiempo de descanso en segundos',
    'camelCase' => 'El caso de Carmel',
    'checkPasswordScore' => 'Comprobar la puntuación de la contraseña',
    'copied' => 'Copiado',
    'countOfPasswords' => 'Recuento de contraseñas generadas',
    'dictionaries' => 'Diccionarios',
    'english' => 'Inglés',
    'french' => 'Francés',
    'generate' => 'Generar',
    'generatedPassword' => 'GENERAR CONTRASEÑA',
    'generatedPasswordAbout' => 'El generador de contraseñas aleatorias le permitirá generar una contraseña que contenga determinados caracteres.',
    'generatedPasswords' => 'Contraseñas generadas',
    'german' => 'Alemán',
    'head' => [
        'meta' => [
            'description' => 'Smart Password le permite generar una contraseña segura que es difícil de descifrar y fácil de recordar',
            'keywords' => 'Inteligente, contraseña, generador, contraseñas',
        ],
        'title' => 'Smart-Password.net - genera tu contraseña!',
    ],
    'italian' => 'Italiano',
    'language' => 'Idioma',
    'languages' => 'Idioma',
    'legendStrengthPasswords' => 'Contraseñas de fuerza de leyenda:',
    'length' => 'Longitud',
    'max' => 'Max',
    'min' => 'Min',
    'passwordCheckScore' => 'Contraseña para comprobar la puntuación',
    'polish' => 'Polaco',
    'result' => 'Resultado',
    'scorePassword' => 'Contraseña de puntuación',
    'scorePasswordAbout' => 'La evaluación de contraseñas le permitirá comprobar la seguridad de una determinada contraseña y estimar el tiempo que llevará descifrarla.',
    'smartPassword' => 'Contraseña inteligente',
    'smartPasswordAbout' => 'La contraseña SMART le permitirá generar una contraseña segura que sea difícil de descifrar y fácil de recordar.',
    'spanish' => 'Español',
    'wait' => 'Esperar',
    'privacyPolicy' => [
        'label' => 'Política de privacidad',
        'slug' => 'política-de-privacidad',
    ],
];
