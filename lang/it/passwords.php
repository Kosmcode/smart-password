<?php

return [
    'numbers' => 'Numeri',
    'rate' => [
        'score' => [
            '0' => 'Significa che la password è estremamente indovinabile (entro 10 ^ 3 ipotesi), parole del dizionario come "password" o "madre"',
            '1' => 'Ancora molto indovinabile (ipotesi < 10^6), un carattere in più su una parola del dizionario',
            '2' => 'Abbastanza indovinabile (ipotesi < 10^8), fornisce una certa protezione da attacchi online non limitati',
            '3' => 'Indovinabile in modo sicuro (ipotesi < 10^10), offre una protezione moderata dallo scenario di hash lento offline',
            '4' => 'Molto impercettibile (ipotesi >= 10^10) e fornisce una forte protezione dallo scenario di hash lento offline',
        ],
        'timePer' => [
            'offline10000000000TriesPerSecond' => 'Offline 10000000000 tentativi al secondo:',
            'offline10000TriesPerSecond' => 'Offline 10000 tentativi al secondo:',
            'online100TriesPerHour' => "Online 100 tentativi all'ora:",
            'online10TriesPerSecond' => 'Online 10 tentativi al secondo:',
        ],
    ],
    'reset' => 'La password è stata reimpostata!',
    'sent' => 'Ti abbiamo inviato una email con il link per il reset della password!',
    'smallLetters' => 'Lettere inferiori',
    'special' => 'Personaggi speciali',
    'throttled' => 'Per favore, attendi prima di riprovare.',
    'token' => 'Questo token di reset della password non è valido.',
    'upperLetters' => 'Lettere superiori',
    'user' => 'Non riusciamo a trovare un utente con questo indirizzo email.',
];
