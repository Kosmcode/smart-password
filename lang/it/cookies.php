<?php

return [
    'consents' => [
        'modal' => [
            'acceptButtonLabel' => 'Accettare',
            'notAcceptButtonLabel' => 'Non accettare',
            'text' => 'Questo sito Web utilizza i cookie per assicurarti di ottenere la migliore esperienza sul nostro sito web.',
            'urlText' => 'Leggi di più sui cookie',
        ],
    ],
];
