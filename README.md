<p align="center">
    <img src="./docs/images/smart-password-logo.png" width="400" alt="Laravel Logo">
</p>

<p align="center">
    <a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Smart Password

Simple Single Page Application to generate passwords

View:
<p align="center">
    <img src="./docs/images/smart-password-view.png" />
</p>

### Tech-stack:
* PHP 8
* Laravel 10
* Vue 3

### Setup:

#### Project
1. `cp .env.example .env` and set vars
2. `docker compose up -d`
3. `docker compose exec --user=sail laravel.test /usr/bin/composer install`
4. `docker compose exec --user=sail laravel.test /usr/bin/npm install`
5. `./vendor/bin/sail down`
6. `./vendor/bin/sail up -d`
7. `./vendor/bin/sail artisan key:generate`
8. `./vendor/bin/sail artisan migrate`
9. `./vendor/bin/sail npm run build`
10. `./vendor/bin/sail artisan optimize:clear`

#### Populate database
* Files with dictionaries are in `./storage/app/dictionaries`

Example for `./storage/app/dictionaries/pl.txt`
1. First prepare adn chunk: `sail artisan dictionary:chunk pl.txt` and other files the same
2. Put it on the queue `sail artisan dictionary:chunked:add-to-queue`
3. Run queue worker: `sail artisan queue:work`

## License

The Smart Password is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
