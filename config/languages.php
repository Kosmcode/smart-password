<?php

use App\Enums\LanguageIdCodeEnum;

return [
    'avaliables' => [
        LanguageIdCodeEnum::en()->label => [
            'prefix' => LanguageIdCodeEnum::en()->label,
            'language' => 'english',
            'iconPrefix' => 'gb',
        ],
        LanguageIdCodeEnum::pl()->label => [
            'prefix' => LanguageIdCodeEnum::pl()->label,
            'language' => 'polish',
            'iconPrefix' => 'pl',
        ],
        LanguageIdCodeEnum::de()->label => [
            'prefix' => LanguageIdCodeEnum::de()->label,
            'language' => 'german',
            'iconPrefix' => 'de',
        ],
        LanguageIdCodeEnum::fr()->label => [
            'prefix' => LanguageIdCodeEnum::fr()->label,
            'language' => 'french',
            'iconPrefix' => 'fr',
        ],
        LanguageIdCodeEnum::it()->label => [
            'prefix' => LanguageIdCodeEnum::it()->label,
            'language' => 'italian',
            'iconPrefix' => 'it',
        ],
        LanguageIdCodeEnum::es()->label => [
            'prefix' => LanguageIdCodeEnum::es()->label,
            'language' => 'spanish',
            'iconPrefix' => 'es',
        ],
    ],
];
