<?php

return [
    'minimumLength' => 5,
    'minimumExpectedSpecialChars' => 3,
    'charToReplace' => [
        'o' => [
            '0',
            //'()',
            //'[]'
        ],
        'O' => [
            '0',
            //'()',
            //'[]',
        ],
        'i' => [
            '!',
            //'/',
            //'\\',
            '1',
        ],
        'I' => [
            '!',
            //'/',
            //'\\',
            '1',
        ],
        't' => [
            '7', //'+'
        ],
        'T' => [
            '7', //'+'
        ],
        'b' => [
            '8',
            //'&',
            //'#',
        ],
        'B' => [
            '8',
            //'&',
            //'#',
        ],
        'a' => [
            '4',
            //'@',
        ],
        'A' => [
            '4',
            //'@',
        ],
        'e' => '3',
        'E' => '3',
        //'y' => '/^',
        //'Y' => '/^',
        'c' => [
            '(',
            //'[',
        ],
        'C' => [
            '(',
            //'['
        ],
        's' => '$',
        'S' => '$',
        //'j' => '_|',
        //'J' => '_|',
        'l' => '|_',
        'L' => '|_',
        //'r' => 'p\\',
        //'R' => 'P\\',
        'v' => '\/',
        'V' => '\/',
    ],
];
