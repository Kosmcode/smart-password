<?php

return [
    'dictionariesPath' => 'dictionaries'.DIRECTORY_SEPARATOR,
    'dictionariesChunkedPath' => 'dictionaries'.DIRECTORY_SEPARATOR.'chunked'.DIRECTORY_SEPARATOR,
    'form' => [
        'camelCaseChecked' => true,
        'passwordsCountAvailableValues' => [20, 40, 60, 80, 100],
        'passwordsCountDefault' => 20,
    ],
];
