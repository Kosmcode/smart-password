<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('words', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('lang_id')
                ->nullable(false)
                ->comment('ID language enum');
            $table->string('original', 255)
                ->nullable(false)
                ->comment('Original word from dictionary');
            $table->string('prepared', 255)
                ->nullable(false)
                ->comment('Prepared word from dictionary');
            $table->tinyInteger('length')
                ->nullable(false)
                ->comment('Length of word');
            $table->tinyInteger('count_special_chars')
                ->nullable(false)
                ->comment('Count of special chars to replace');
            $table->string('available_spec_chars', 255)
                ->nullable(false)
                ->comment('Available special chars to use in word');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('words');
    }
};
